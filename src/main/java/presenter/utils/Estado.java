package presenter.utils;

public enum Estado {
    TAB_ARCHIVOS, TAB_MATERIAS, TAB_FILTROS, TAB_COMBINACIONES
}

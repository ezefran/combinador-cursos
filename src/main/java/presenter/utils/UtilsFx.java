package presenter.utils;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import model.domain.combinacion.curso.Curso;
import model.domain.combinacion.curso.horario.Hora;
import model.service.ColoresService;

import java.io.File;
import java.util.List;

public class UtilsFx {

    public static File abrirArchivo(Node node, String... extensiones) {
        var desc = String.join(",", extensiones);
        var fc = new FileChooser();
        fc.setTitle("Seleccionar archivo");
        for (var ext : extensiones) {
            fc.getExtensionFilters().add(new ExtensionFilter(desc, ext));
        }
        return fc.showOpenDialog(node.getScene().getWindow());
    }

    public static File guardarArchivo(Node node, String... extensiones) {
        var desc = String.join(",", extensiones);
        var fc = new FileChooser();
        fc.setTitle("Guardar archivo");
        fc.setInitialFileName("*.json");
        for (var ext : extensiones) {
            fc.getExtensionFilters().add(new ExtensionFilter(desc, ext));
        }
        return fc.showSaveDialog(node.getScene().getWindow());
    }

    public static ButtonType alertaInfo(String titulo, String texto) {
        var alerta = new Alert(Alert.AlertType.INFORMATION, texto, ButtonType.CLOSE);
        alerta.setHeaderText(null);
        alerta.setTitle(titulo);
        alerta.getDialogPane().getStylesheets().add("/styles/dialog.css");
        alerta.showAndWait();
        return alerta.getResult();
    }

    public static void reiniciarTabla(GridPane tabla, Node[][] nodosTabla) {
        tabla.getChildren()
                .removeIf(nodo -> nodo.getStyleClass().contains("MATERIA-TABLA")
                        || nodo.getStyleClass().contains("GRID-BODY-CELL"));
        for (int i = 1; i <= 6; ++i) {
            for (int j = 1; j <= 7; ++j) {
                var label = new Label();
                var celda = new AnchorPane(label);
                celda.getStyleClass().add("GRID-BODY-CELL");
                tabla.add(celda, i, j);
                nodosTabla[i - 1][j - 1] = celda;
            }
        }
    }

    public static void reemplazarNodoTabla(GridPane tabla, Node[][] nodosTabla, Node nodoNuevo, int pos_x, int pos_y) {
        tabla.getChildren().remove(nodosTabla[pos_x - 1][pos_y - 1]);
        nodosTabla[pos_x - 1][pos_y - 1] = nodoNuevo;
        tabla.add(nodoNuevo, pos_x, pos_y);
    }

    public static void agregarCursosEnTabla(ColoresService coloresService, GridPane tabla, Node[][] nodosTabla, List<Curso> cursos) {
        for (var curso : cursos) {
            for (var horario : curso.getHorarios()) {
                int x = horario.getDia().ordinal() + 1;
                for (var pos : Hora.getPosiciones(horario.getHoras())) {
                    int y = pos + 1;

                    var label = new Label(curso.getMateria().getSigla());
                    var nodo = new AnchorPane(label);

                    AnchorPane.setTopAnchor(label, 0.0);
                    AnchorPane.setLeftAnchor(label, 0.0);
                    AnchorPane.setBottomAnchor(label, 0.0);
                    AnchorPane.setRightAnchor(label, 0.0);
                    label.setAlignment(Pos.CENTER);
                    var color = coloresService.obtenerColorAsignado(curso);
                    nodo.setStyle("-fx-background-color: " + color);
                    nodo.getStyleClass().add("MATERIA-TABLA");

                    reemplazarNodoTabla(tabla, nodosTabla, nodo, x, y);
                }
            }
        }
    }
}

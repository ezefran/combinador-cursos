package presenter.impl;

import model.service.MateriasService;
import presenter.TabMateriasAprobadasPresenter;
import view.impl.DefaultTabMateriasAprobadasView;

public class DefaultTabMateriasAprobadasPresenter implements TabMateriasAprobadasPresenter {

    private final MateriasService materiasService;

    public DefaultTabMateriasAprobadasPresenter(MateriasService materiasService) {
        this.materiasService = materiasService;
    }

    @Override
    public void iniciar(DefaultTabMateriasAprobadasView view) {
        var listaMaterias = materiasService.buscarListaMaterias();
        var materiasAprobadas = materiasService.buscarMateriasAprobadas();
        view.mostrarMaterias(listaMaterias, materiasAprobadas);
    }
}

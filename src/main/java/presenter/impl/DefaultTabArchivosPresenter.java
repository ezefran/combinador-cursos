package presenter.impl;

import model.domain.Carrera;
import model.domain.Cuatrimestre;
import model.domain.Universidad;
import model.domain.parser.ParametrosParser;
import model.service.MateriasService;
import presenter.MainPresenter;
import presenter.TabArchivosPresenter;
import view.MainView;

import java.io.File;

public class DefaultTabArchivosPresenter implements TabArchivosPresenter {

    private final MateriasService materiasService;

    public DefaultTabArchivosPresenter(MateriasService materiasService) {
        this.materiasService = materiasService;
    }

    @Override
    public void iniciar() {

    }

    @Override
    public void onCargarArchivos(MainPresenter mainPresenter, MainView mainView, File archivoListaMaterias, File archivoOfertaMaterias, Universidad universidad, Carrera carrera, Cuatrimestre cuatrimestre) {
        ParametrosParser parametrosParser = new ParametrosParser(universidad, carrera, cuatrimestre);
        materiasService.cargarListaMaterias(parametrosParser, archivoListaMaterias);
        materiasService.cargarMateriasOfertadas(parametrosParser, archivoOfertaMaterias);
        mainPresenter.onCargarArchivos(mainView);
    }
}

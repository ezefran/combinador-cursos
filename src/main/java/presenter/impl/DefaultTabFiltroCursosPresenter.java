package presenter.impl;

import model.domain.combinacion.curso.Curso;
import model.service.CursosService;
import model.service.MateriasService;
import presenter.TabFiltroCursosPresenter;
import view.MainView;
import view.TabFiltroCursosView;
import view.components.CursoCursableListCellView;

public class DefaultTabFiltroCursosPresenter implements TabFiltroCursosPresenter {

    private final MateriasService materiasService;
    private final CursosService cursosService;

    public DefaultTabFiltroCursosPresenter(MateriasService materiasService, CursosService cursosService) {
        this.materiasService = materiasService;
        this.cursosService = cursosService;
    }

    @Override
    public void iniciar(TabFiltroCursosView tabFiltroCursosView) {
        var materiasCursables = materiasService.obtenerMateriasCursables();
        var cursos = cursosService.obtenerCursosCursables(materiasCursables);
        tabFiltroCursosView.mostrarCursos(cursos);
    }

    @Override
    public void onClickCurso(MainView mainView, TabFiltroCursosPresenter tabFiltroCursosPresenter, Curso curso) {
        if (!cursosService.buscarCursosSeleccionados().contains(curso)) {
            cursosService.agregarCursoASeleccionados(curso);
        } else {
            cursosService.removerDeCursosSeleccionados(curso);
        }
        mainView.deshabilitarItemCombinaciones();
    }

    @Override
    public void onUpdateItem(CursoCursableListCellView view, Curso curso) {
        if (cursosService.buscarCursosSeleccionados().contains(curso)) {
            view.seleccionar(true);
        } else {
            view.seleccionar(false);
        }
    }
}

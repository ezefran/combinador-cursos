package presenter.impl;

import model.service.ColoresService;
import model.service.CombinacionService;
import model.service.CursosService;
import presenter.TabCombinacionesPresenter;
import presenter.constants.Valores;
import view.TabCombinacionesView;

public class DefaultTabCombinacionesPresenter implements TabCombinacionesPresenter {


    private final CombinacionService combinacionService;
    private final CursosService cursosService;
    private final ColoresService coloresService;

    public DefaultTabCombinacionesPresenter(CombinacionService combinacionService, CursosService cursosService, ColoresService coloresService) {
        this.combinacionService = combinacionService;
        this.cursosService = cursosService;
        this.coloresService = coloresService;
    }

    @Override
    public void iniciar(TabCombinacionesView tabCombinacionesView) {
        var cursosCombinacion = combinacionService.buscarCursosCombinacion();
        var cursosSeleccionados = cursosService.buscarCursosSeleccionados();
        var combinaciones = combinacionService.buscarCombinaciones(cursosSeleccionados, Valores.MAX_CURSOS_POR_COMBINACION);
        tabCombinacionesView.iniciarListaCombinaciones(coloresService, combinaciones);
        tabCombinacionesView.iniciarListaCursosCombinacion(coloresService, cursosCombinacion);
    }
}

package presenter.impl;

import model.domain.combinacion.curso.TipoCurso;
import model.domain.combinacion.curso.horario.Dia;
import model.service.ArchivosService;
import model.service.FiltroCombinacionService;
import presenter.MainPresenter;
import presenter.utils.Estado;
import view.MainView;
import view.TabFiltroCursosView;

import java.io.File;
import java.util.List;

public class DefaultMainPresenter implements MainPresenter {

    private final ArchivosService archivosService;
    private final FiltroCombinacionService filtroCombinacionService;

    private Estado estadoActual;

    public DefaultMainPresenter(ArchivosService archivosService, FiltroCombinacionService filtroCombinacionService) {
        this.archivosService = archivosService;
        this.filtroCombinacionService = filtroCombinacionService;
    }

    @Override
    public void iniciar(MainView mainView) {
        estadoActual = Estado.TAB_ARCHIVOS;
        cambiarTab(mainView, Estado.TAB_ARCHIVOS);
    }

    @Override
    public void onGoToCargarArchivos(MainView mainView) {
        cambiarTab(mainView, Estado.TAB_ARCHIVOS);
    }

    @Override
    public void onGoToSeleccionarMaterias(MainView mainView) {
        cambiarTab(mainView, Estado.TAB_MATERIAS);
    }

    @Override
    public void onGoToFiltrarCursos(MainView mainView) {
        cambiarTab(mainView, Estado.TAB_FILTROS);
    }

    @Override
    public void onGoToVerCombinaciones(MainView mainView) {
        cambiarTab(mainView, Estado.TAB_COMBINACIONES);
    }

    private void cambiarTab(MainView mainView, Estado nuevoEstado) {
        switch (estadoActual) {
            case TAB_ARCHIVOS:
                mainView.cerrarTabArchivos();
                break;
            case TAB_MATERIAS:
                mainView.cerrarTabMaterias();
                break;
            case TAB_FILTROS:
                mainView.cerrarTabFiltrarCursos();
                break;
            case TAB_COMBINACIONES:
                mainView.cerrarTabCombinaciones();
                break;
        }
        switch (nuevoEstado) {
            case TAB_ARCHIVOS:
                estadoActual = Estado.TAB_ARCHIVOS;
                mainView.abrirTabArchivos();
                break;
            case TAB_MATERIAS:
                estadoActual = Estado.TAB_MATERIAS;
                mainView.abrirTabMaterias();
                break;
            case TAB_FILTROS:
                estadoActual = Estado.TAB_FILTROS;
                mainView.abrirTabFiltrarCursos();
                break;
            case TAB_COMBINACIONES:
                estadoActual = Estado.TAB_COMBINACIONES;
                mainView.abrirTabCombinaciones();
                break;
        }
    }

    @Override
    public void onSubirMateriasAprobadas(MainView mainView, File archivo) {
        archivosService.cargarMateriasAprobadas(archivo);
        cambiarTab(mainView, Estado.TAB_FILTROS);
    }

    @Override
    public void onBajarMateriasAprobadas(MainView mainView, File archivo) {
        archivosService.guardarMateriasAprobadas(archivo);
    }

    @Override
    public void onClickFiltro(TabFiltroCursosView tabFiltroCursosView, List<Dia> diasActivos, int horasActivas, List<TipoCurso> tiposActivos) {
        filtroCombinacionService.filtrarCursosCursables(diasActivos, horasActivas, tiposActivos);
        tabFiltroCursosView.refrescarListaCursos();
    }

    @Override
    public void onSalir(MainView mainView) {

    }

    @Override
    public void onConfirmarCursosSeleccionados(MainView mainView) {
        cambiarTab(mainView, Estado.TAB_COMBINACIONES);
        mainView.habilitarItemCombinaciones();
    }

    @Override
    public void onCargarArchivos(MainView mainView) {
        cambiarTab(mainView, Estado.TAB_MATERIAS);
        mainView.habilitarItemSeleccionarMaterias();
    }

    @Override
    public void onCambiosOpcionesCargarArchivos(MainView mainView) {
        mainView.deshabilitarItemSeleccionarMaterias();
        mainView.deshabilitarItemFiltrarCursos();
        mainView.deshabilitarItemCombinaciones();
    }

    @Override
    public void onConfirmarMaterias(MainView mainView) {
        cambiarTab(mainView, Estado.TAB_FILTROS);
        mainView.habilitarItemFiltrarCursos();
    }
}

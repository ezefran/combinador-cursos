package presenter;

import view.TabCombinacionesView;

public interface TabCombinacionesPresenter {
    void iniciar(TabCombinacionesView tabCombinacionesView);
}

package presenter;

import model.domain.combinacion.curso.TipoCurso;
import model.domain.combinacion.curso.horario.Dia;
import view.MainView;
import view.TabFiltroCursosView;

import java.io.File;
import java.util.List;

public interface MainPresenter {
    void iniciar(MainView mainView);

    void onGoToCargarArchivos(MainView mainView);
    void onGoToSeleccionarMaterias(MainView mainView);
    void onGoToFiltrarCursos(MainView mainView);
    void onGoToVerCombinaciones(MainView mainView);

    void onClickFiltro(TabFiltroCursosView mainView, List<Dia> diasActivos, int horasActivas, List<TipoCurso> tiposActivos);

    void onSubirMateriasAprobadas(MainView mainView, File archivo);
    void onBajarMateriasAprobadas(MainView mainView, File archivo);

    void onSalir(MainView mainView);

    void onCargarArchivos(MainView mainView);
    void onCambiosOpcionesCargarArchivos(MainView mainView);
    void onConfirmarMaterias(MainView mainView);
    void onConfirmarCursosSeleccionados(MainView mainView);
}

package presenter;

import view.impl.DefaultTabMateriasAprobadasView;

public interface TabMateriasAprobadasPresenter {
    void iniciar(DefaultTabMateriasAprobadasView tabMateriasAprobadasView);
}

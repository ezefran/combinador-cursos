package presenter;

import model.domain.combinacion.curso.Curso;
import view.MainView;
import view.TabFiltroCursosView;
import view.components.CursoCursableListCellView;

public interface TabFiltroCursosPresenter {
    void iniciar(TabFiltroCursosView tabFiltroCursosView);
    void onClickCurso(MainView mainView, TabFiltroCursosPresenter tabFiltroCursosPresenter, Curso curso);
    void onUpdateItem(CursoCursableListCellView view, Curso curso);
}

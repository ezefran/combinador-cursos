package presenter;

import model.domain.Carrera;
import model.domain.Cuatrimestre;
import model.domain.Universidad;
import view.MainView;

import java.io.File;

public interface TabArchivosPresenter {
    void iniciar();
    void onCargarArchivos(MainPresenter mainPresenter, MainView mainView, File archivoListaMaterias, File archivoOfertaMaterias, Universidad universidad, Carrera carrera, Cuatrimestre cuatrimestre);
}

package model.service;

import model.domain.combinacion.Combinacion;
import model.domain.combinacion.curso.Curso;
import model.repository.CombinacionesRepository;

import java.util.ArrayList;
import java.util.List;

public class CombinacionService {

    private final CombinacionesRepository combinacionesRepository;

    public CombinacionService(CombinacionesRepository combinacionesRepository) {
        this.combinacionesRepository = combinacionesRepository;
    }

    public List<Curso> buscarCursosCombinacion() {
        return combinacionesRepository.getCursosCombinacion();
    }

    public List<Combinacion> buscarCombinaciones(List<Curso> cursos, int maxCursosPorCombinacion) {
        var combinaciones = new ArrayList<Combinacion>();

        for (var curso : cursos) {
            var combinacionActual = new Combinacion(curso);
            combinaciones.add(combinacionActual);
            var iteradorCombinaciones = combinaciones.listIterator(0);

            while (iteradorCombinaciones.hasNext()) {
                var combinacion = iteradorCombinaciones.next();
                if (combinacion != combinacionActual) {
                    if (combinacionActual.getSize() + combinacion.getSize() <= maxCursosPorCombinacion
                            && !combinacionActual.hayConflictoDeHorario(combinacion)) {
                        if (!Combinacion.contienenMismaMateria(combinacion, combinacionActual)) {
                            iteradorCombinaciones.add(new Combinacion(combinacion, combinacionActual));
                        }
                    }
                }
            }
        }

        combinaciones.sort((c1, c2) -> {
            int cursos1 = c1.getCursos().size();
            int cursos2 = c2.getCursos().size();
            return Integer.compare(cursos1, cursos2) * -1;
        });

        return combinaciones;
    }
}

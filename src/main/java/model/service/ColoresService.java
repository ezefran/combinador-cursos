package model.service;

import model.domain.combinacion.curso.Curso;

import java.util.HashMap;

public class ColoresService {

    private final HashMap<Curso, String> colorCurso;
    private final String[] colores = {
            "#EFAFAF", "#EFC5AF", "#EFD6AF", "#EFE9AF", "#CFEFAF", "#AFEFBA",
            "#AFEFE2", "#AFD6EF", "#AFB8EF", "#BCAFEF", "#D1AFEF", "#EFAFEB",
            "#EFAFC7", "#AFEFE6"
    };

    private int color = 0;

    public ColoresService() {
        colorCurso = new HashMap<>();
    }

    public String obtenerColorAsignado(Curso curso) {
        if (!colorCurso.containsKey(curso)) {
            asignarColor(curso);
        }
        return colorCurso.get(curso);
    }

    private void asignarColor(Curso curso) {
        var nuevoColor = color++ % 14;
        colorCurso.put(curso, colores[nuevoColor]);
    }
}

package model.service;

import model.domain.parser.ParserJSON;
import model.repository.MateriasRepository;

import java.io.File;

public class ArchivosService {

    private final MateriasRepository materiasRepository;

    public ArchivosService(MateriasRepository materiasRepository) {
        this.materiasRepository = materiasRepository;
    }

    public void guardarMateriasAprobadas(File archivo) {
        ParserJSON.guardarMateriasAprobadas(materiasRepository.getMateriasAprobadas(), archivo);
    }

    public void cargarMateriasAprobadas(File archivo) {
        var materias = ParserJSON.cargarMateriasAprobadas(archivo);
        materiasRepository.setMateriasAprobadas(materias);
    }
}

package model.service;

import model.domain.combinacion.curso.Curso;
import model.domain.combinacion.curso.Materia;
import model.repository.CursosRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CursosService {

    private final CursosRepository cursosRepository;

    public CursosService(CursosRepository cursosRepository) {
        this.cursosRepository = cursosRepository;
    }

    public List<Curso> obtenerCursosCursables(List<Materia> materiasCursables) {
        List<Curso> cursosMateriasCursables = new ArrayList<>();
        for (var materia : materiasCursables) {
            cursosMateriasCursables.addAll(materia.getCursos());
        }

        cursosMateriasCursables.sort(Comparator.comparingInt(c -> c.getMateria().getCodigo()));
        cursosRepository.setCursosDisponibles(new ArrayList<>(cursosMateriasCursables));
        return cursosRepository.getCursosDisponibles();
    }

    public List<Curso> buscarCursosDisponibles() {
        return Collections.unmodifiableList(cursosRepository.getCursosDisponibles());
    }

    public void agregarCursoADisponibles(Curso curso) {
        cursosRepository.getCursosDisponibles().add(curso);
    }

    public void removerDeCursosDisponibles(Curso curso) {
        cursosRepository.getCursosDisponibles().remove(curso);
    }

    public List<Curso> buscarCursosSeleccionados() {
        return Collections.unmodifiableList(cursosRepository.getCursosSeleccionados());
    }

    public void agregarCursoASeleccionados(Curso curso) {
        cursosRepository.getCursosSeleccionados().add(curso);
    }

    public void removerDeCursosSeleccionados(Curso curso) {
        cursosRepository.getCursosSeleccionados().remove(curso);
    }

    public void removerDeCursosSeleccionados(List<Curso> cursos) {
        cursosRepository.getCursosSeleccionados().removeAll(cursos);
    }

    public List<Curso> buscarCursosFiltrados() {
        return Collections.unmodifiableList(cursosRepository.getCursosFiltrados());
    }

    public void agregarCursoAFiltrados(Curso curso) {
        cursosRepository.getCursosFiltrados().add(curso);
    }

    public void removerDeCursosFiltrados(Curso curso) {
        cursosRepository.getCursosFiltrados().remove(curso);
    }
}

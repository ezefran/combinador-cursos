package model.service;

import model.domain.combinacion.curso.Curso;
import model.domain.combinacion.curso.Materia;
import model.domain.parser.ParametrosParser;
import model.domain.parser.ParserFactory;
import model.repository.MateriasRepository;

import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class MateriasService {

    private final MateriasRepository materiasRepository;

    public MateriasService(MateriasRepository materiasRepository) {
        this.materiasRepository = materiasRepository;
    }

    public List<Materia> buscarListaMaterias() {
        return materiasRepository.getListaMaterias();
    }

    public List<Materia> buscarMateriasAprobadas() {
        return materiasRepository.getMateriasAprobadas();
    }

    public void cargarListaMaterias(ParametrosParser parametrosParser, File archivoListaMaterias) {
        var parser = ParserFactory.getParser(parametrosParser);
        var listaMaterias = parser.extraerListaMaterias(archivoListaMaterias);
        materiasRepository.setListaMaterias(listaMaterias);

        var materiasOfertadas = materiasRepository.getMateriasOfertadas();
        corregirMaterias(listaMaterias, materiasOfertadas);
    }

    public void cargarMateriasOfertadas(ParametrosParser parametrosParser, File archivoOfertaMaterias) {
        var parser = ParserFactory.getParser(parametrosParser);
        var materiasOfertadas = parser.extraerMateriasOfertadas(archivoOfertaMaterias);
        materiasRepository.setMateriasOfertadas(materiasOfertadas);

        var listaMaterias = materiasRepository.getListaMaterias();
        corregirMaterias(listaMaterias, materiasOfertadas);
    }

    private void corregirMaterias(List<Materia> listaMaterias, List<Materia> materiasOfertadas) {
        if (listaMaterias.size() == 0 || materiasOfertadas.size() == 0) return;
        corregirCodigos(listaMaterias, materiasOfertadas);
        removerMateriasSinCodigo(listaMaterias);
        listaMaterias.sort(Comparator.comparing(Materia::getCodigo));
    }

    public void corregirCodigos(List<Materia> materiasAcorregir, List<Materia> materiasConCodigoCorrecto) {
        for (Materia materia : materiasAcorregir) {
            materia.corregirCodigo(materiasConCodigoCorrecto);
        }
    }

    public void removerMateriasSinCodigo(List<Materia> materias) {
        materias.removeIf(materia -> !materia.tieneCodigo());
    }

    public List<Materia> obtenerMateriasCursables() {
        List<Materia> listaMaterias = materiasRepository.getListaMaterias();
        List<Materia> materiasAprobadas = materiasRepository.getMateriasAprobadas();
        List<Materia> materiasOfertadas = materiasRepository.getMateriasOfertadas();

        List<Materia> materias = new ArrayList<>();
        for (Materia materia : listaMaterias) {
            if (!materiasOfertadas.contains(materia)) {
                continue;
            }
            materia.setCursos(obtenerCursosOfertados(materia, materiasOfertadas));
            if (!materias.contains(materia)) {
                if (materia.getCorrelatividad() == null) {
                    if (!materiasAprobadas.contains(materia)) {
                        materias.add(materia);
                    }
                } else {
                    if (!materiasAprobadas.contains(materia)
                            && materiasRequeridasAprobadas(materia, materiasAprobadas)) {
                        materias.add(materia);
                    }
                }
            }
        }
        return materias;
    }

    private List<Curso> obtenerCursosOfertados(Materia materia, List<Materia> materiasOfertadas) {
        int index = materiasOfertadas.indexOf(materia);
        return materiasOfertadas.get(index).getCursos();
    }

    private boolean materiasRequeridasAprobadas(Materia materia, List<Materia> materiasAprobadas) {
        int requeridasAprobadas = 0;
        for (Integer correlatividad : materia.getCorrelatividad()) {
            for (Materia materiaAprobada : materiasAprobadas) {
                if (correlatividad == materiaAprobada.getCodigo()) {
                    requeridasAprobadas++;
                }
            }
        }
        return requeridasAprobadas == materia.getCorrelatividad().size();
    }
}

package model.service;

import model.domain.combinacion.curso.Curso;
import model.domain.combinacion.curso.TipoCurso;
import model.domain.combinacion.curso.horario.Dia;
import model.domain.combinacion.curso.horario.Hora;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FiltroCombinacionService {

    private final CursosService cursosService;

    public FiltroCombinacionService(CursosService cursosService) {
        this.cursosService = cursosService;
    }

    public void filtrarCursosCursables(List<Dia> diasActivos, int horasActivas, List<TipoCurso> tiposActivos) {
        var listaCursosARemover = new ArrayList<Curso>();
        var listaCursosAAgregar = new ArrayList<Curso>();

        List<Curso> todosLosCursos = Stream.of(cursosService.buscarCursosDisponibles(), cursosService.buscarCursosFiltrados())
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

        for (Curso curso : todosLosCursos) {
            if (tiposActivosNoContienenTipoDelCurso(curso, tiposActivos)
                    || horariosActivosNoContienenAlMenosUnHorarioDelCurso(curso, diasActivos, horasActivas)) {
                listaCursosARemover.add(curso);
            } else {
                listaCursosAAgregar.add(curso);
            }
        }

        listaCursosARemover.forEach(curso -> removerCursoCursable(curso));
        listaCursosAAgregar.forEach(curso -> agregarCursoCursable(curso));

        cursosService.removerDeCursosSeleccionados(listaCursosARemover);
    }

    private boolean tiposActivosNoContienenTipoDelCurso(Curso curso, List<TipoCurso> tiposActivos) {
        return !tiposActivos.contains(curso.getTipo());
    }

    private boolean horariosActivosNoContienenAlMenosUnHorarioDelCurso(Curso curso, List<Dia> diasActivos, int horasActivas) {
        return curso.getHorarios().stream()
                .anyMatch(horario -> !diasActivos.contains(horario.getDia())
                        || !Hora.contiene(horasActivas, horario.getHoras()));
    }

    private void agregarCursoCursable(Curso curso) {
        cursosService.removerDeCursosFiltrados(curso);
        if (!cursosService.buscarCursosDisponibles().contains(curso)) {
            cursosService.agregarCursoADisponibles(curso);
        }
    }

    private void removerCursoCursable(Curso curso) {
        cursosService.removerDeCursosDisponibles(curso);
        if (!cursosService.buscarCursosFiltrados().contains(curso)) {
            cursosService.agregarCursoAFiltrados(curso);
        }
    }
}

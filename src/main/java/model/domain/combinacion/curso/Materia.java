package model.domain.combinacion.curso;

import java.util.ArrayList;
import java.util.List;

public class Materia {

    private final String nombre;
    private int codigo;
    private List<Integer> correlatividad;

    private transient List<Curso> cursos;

    public Materia(int codigo, String nombre) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.cursos = new ArrayList<>();
    }

    public Materia(int codigo, String nombre, List<Integer> correlatividad) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.cursos = new ArrayList<>();
        this.correlatividad = correlatividad;
    }

    public int getCodigo() {
        return codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public List<Curso> getCursos() {
        return cursos;
    }

    public void setCursos(List<Curso> cursos) {
        this.cursos = cursos;
    }

    public List<Integer> getCorrelatividad() {
        return correlatividad;
    }

    public boolean tieneCodigo() {
        return codigo != 0;
    }

    public String getSigla() {
        StringBuilder sigla = new StringBuilder();
        for (var palabra : nombre.split(" ")) {
            palabra = palabra.toUpperCase();
            if (palabra.contains("DE") || palabra.contains("Y")) continue;
            sigla.append(palabra, 0, 1);
        }
        return sigla.toString();
    }

    public void addCurso(Curso curso) {
        if (!cursos.contains(curso)) {
            cursos.add(curso);
            curso.setMateria(this);
        }
    }

    public void corregirCodigo(List<Materia> materiasConCodigoCorrecto) {
        if (tieneCodigo()) return;
        for (Materia materia : materiasConCodigoCorrecto) {
            if (nombre.equals(materia.nombre)) {
                codigo = materia.codigo;
            }
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Materia)) return false;
        Materia otraMateria = (Materia) obj;
        return codigo == otraMateria.codigo
                && nombre.equals(otraMateria.nombre);
    }

    @Override
    public String toString() {
        return "Materia{" +
                "nombre='" + nombre + '\'' +
                ", codigo=" + codigo +
                ", correlatividad=" + correlatividad +
                ", cursos=" + cursos +
                '}';
    }
}

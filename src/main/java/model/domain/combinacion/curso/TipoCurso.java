package model.domain.combinacion.curso;

public enum TipoCurso {
    PRESENCIAL, SEMI_PRESENCIAL, A_DISTANCIA
}

package model.domain.combinacion.curso.horario;

import java.util.ArrayList;
import java.util.List;

public class Hora {
    public static final int M08a10 = 1;
    public static final int M10a12 = 2;
    public static final int M08a12 = 3;

    public static final int T12a14 = 4;
    public static final int T14a16 = 8;
    public static final int T13a17 = 12;
    public static final int T16a18 = 16;
    public static final int T14a18 = 24;

    public static final int N19a21 = 32;
    public static final int N21a23 = 64;
    public static final int N19a23 = 96;

    public static final String[] horariosStrArr = {
            "08a10", "10a12", "08a12",
            "12a14", "14a16", "13a17", "16a18", "14a18",
            "19a21", "21a23", "19a23"
    };

    public static final int[] horariosArr = {
            M08a10, M10a12, M08a12,
            T12a14, T14a16, T13a17, T16a18, T14a18,
            N19a21, N21a23, N19a23
    };

    public static List<Integer> getPosiciones(int horas) {
        var posiciones = new ArrayList<Integer>();
        if (contiene(horas, M08a12)) {
            posiciones.add(0);
        }
        if (contiene(horas, M10a12)) {
            posiciones.add(1);
        }
        if (contiene(horas, T12a14)) {
            posiciones.add(2);
        }
        if (contiene(horas, T14a16)) {
            posiciones.add(3);
        }
        if (contiene(horas, T16a18)) {
            posiciones.add(4);
        }
        if (contiene(horas, N19a21)) {
            posiciones.add(5);
        }
        if (contiene(horas, N21a23)) {
            posiciones.add(6);
        }
        return posiciones;
    }

    public static boolean contiene(int horas1, int horas2) {
        return (horas1 & horas2) == horas2;
    }
}

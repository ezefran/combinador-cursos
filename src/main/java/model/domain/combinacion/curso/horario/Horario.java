package model.domain.combinacion.curso.horario;

public class Horario {

    private final Dia dia;
    private final int horas;

    public Horario(Dia dia, int horas) {
        this.dia = dia;
        this.horas = horas;
    }

    public Dia getDia() {
        return dia;
    }

    public int getHoras() {
        return horas;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Horario)) return false;
        var otroHorario = (Horario) obj;
        if (horas != otroHorario.horas) return false;
        return dia == otroHorario.dia;
    }
}

package model.domain.combinacion.curso;

import model.domain.combinacion.Combinable;
import model.domain.combinacion.Combinacion;
import model.domain.combinacion.curso.horario.Horario;
import model.utils.Conversor;

import java.util.List;

public class Curso implements Combinable {

    private final int codigo;
    private final List<Horario> horarios;
    private final TipoCurso tipo;

    private transient Materia materia;

    public Curso(int codigo, List<Horario> horarios, TipoCurso tipo) {
        this.codigo = codigo;
        this.horarios = horarios;
        this.tipo = tipo;
    }

    public Curso(int codigo, Horario horario, TipoCurso tipo) {
        this.codigo = codigo;
        this.horarios = List.of(horario);
        this.tipo = tipo;
    }

    public int getCodigo() {
        return codigo;
    }

    public List<Horario> getHorarios() {
        return horarios;
    }

    public TipoCurso getTipo() {
        return tipo;
    }

    public Materia getMateria() {
        return materia;
    }

    public void setMateria(Materia materia) {
        this.materia = materia;
    }

    public String getHorariosAsString() {
        return Conversor.horariosToString(horarios);
    }

    public String getTipoCursoAsString() {
        return Conversor.tipoCursoToString(tipo);
    }

    @Override
    public boolean hayConflictoDeHorario(Curso otroCurso) {
        if (tipo == TipoCurso.A_DISTANCIA || otroCurso.tipo == TipoCurso.A_DISTANCIA) return false;
        return hayConflictoDeHorario(otroCurso.horarios);
    }

    @Override
    public boolean hayConflictoDeHorario(Combinacion combinacion) {
        return combinacion.hayConflictoDeHorario(this);
    }

    private boolean hayConflictoDeHorario(List<Horario> otrosHorarios) {
        for (var horario : horarios) {
            boolean hayConflictoDeHorario = otrosHorarios.stream()
                    .anyMatch(otroHorario -> otroHorario.getDia() == horario.getDia()
                            && (otroHorario.getHoras() & horario.getHoras()) != 0);
            if (hayConflictoDeHorario) return true;
        }
        return false;
    }
}

package model.domain.combinacion;

import model.domain.combinacion.curso.Curso;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Combinacion implements Combinable {

    private final Combinable combinable1;
    private final Combinable combinable2;
    private List<Curso> cacheCursos;

    public Combinacion(Combinable c1, Combinable c2) {
        combinable1 = c1;
        combinable2 = c2;
    }

    public Combinacion(Curso c1) {
        combinable1 = c1;
        combinable2 = null;
    }

    public static boolean contienenMismaMateria(Combinacion combinacion1, Combinacion combinacion2) {
        for (var curso1 : combinacion1.getCursos()) {
            for (var curso2 : combinacion2.getCursos()) {
                if (curso1.getMateria().getCodigo() == curso2.getMateria().getCodigo()) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public boolean hayConflictoDeHorario(Curso curso) {
        return getCursos().stream()
                .anyMatch(cursoDeLaCombinacion -> cursoDeLaCombinacion.hayConflictoDeHorario(curso));
    }

    @Override
    public boolean hayConflictoDeHorario(Combinacion otraCombinacion) {
        for (Curso cursoDeLaCombinacion : getCursos()) {
            boolean combinacionesEnConflicto = otraCombinacion.getCursos().stream()
                    .anyMatch(cursoDeLaCombinacion::hayConflictoDeHorario);
            if (combinacionesEnConflicto) return true;
        }
        return false;
    }

    public int getSize() {
        return getCursos().size();
    }

    public List<Curso> getCursos() {
        if (cacheCursos == null) {
            cacheCursos = new ArrayList<>();
            listarCursos(cacheCursos);
            cacheCursos.sort(Comparator.comparingInt(c -> c.getMateria().getCodigo()));
        }
        return cacheCursos;
    }

    /***
     * @param lista donde se guardan los cursos de la combinacion
     */
    private void listarCursos(List<Curso> lista) {
        if (combinable1 instanceof Combinacion) {
            ((Combinacion) combinable1).listarCursos(lista);
        }
        if (combinable2 instanceof Combinacion) {
            ((Combinacion) combinable2).listarCursos(lista);
        }
        if (combinable1 instanceof Curso) lista.add((Curso) combinable1);
        if (combinable2 instanceof Curso) lista.add((Curso) combinable2);
    }

    @Override
    public String toString() {
        List<String> materias = getCursos().stream()
                .map(curso -> curso.getMateria().getSigla())
                .collect(Collectors.toList());

        StringBuilder sb = new StringBuilder();
        int ultimoIndice = materias.size() - 1;
        for (int i = 0; i < materias.size(); i++) {
            sb.append(materias.get(i));
            if (i != ultimoIndice) {
                sb.append(", ");
            }
        }

        return "(" + sb.toString() + ")";
    }
}

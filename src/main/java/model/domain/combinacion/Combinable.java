package model.domain.combinacion;

import model.domain.combinacion.curso.Curso;

public interface Combinable {
    boolean hayConflictoDeHorario(Curso curso);
    boolean hayConflictoDeHorario(Combinacion combinacion);
}

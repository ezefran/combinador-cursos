package model.domain.parser;

import model.domain.Carrera;
import model.domain.Cuatrimestre;
import model.domain.Universidad;

public class ParametrosParser {

    private final Universidad universidad;
    private final Carrera carrera;
    private final Cuatrimestre cuatrimestre;

    public ParametrosParser(Universidad universidad, Carrera carrera, Cuatrimestre cuatrimestre) {
        this.universidad = universidad;
        this.carrera = carrera;
        this.cuatrimestre = cuatrimestre;
    }

    public Universidad getUniversidad() {
        return universidad;
    }

    public Carrera getCarrera() {
        return carrera;
    }

    public Cuatrimestre getCuatrimestre() {
        return cuatrimestre;
    }
}

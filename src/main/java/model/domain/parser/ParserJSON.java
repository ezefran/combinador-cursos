package model.domain.parser;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import model.domain.combinacion.curso.Materia;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ParserJSON {

    public static void guardarMateriasAprobadas(List<Materia> materiasAprobadas, File archivo) {
        Gson g = new GsonBuilder().setPrettyPrinting().create();
        try {
            var writer = new FileWriter(archivo);
            g.toJson(materiasAprobadas, writer);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<Materia> cargarMateriasAprobadas(File archivo) {
        try {
            Gson g = new Gson();
            FileReader reader = new FileReader(archivo);
            var materiasType = new TypeToken<List<Materia>>() {}.getType();
            List<Materia> materiasAprobadas = new ArrayList<>(g.fromJson(reader, materiasType));
            reader.close();
            return materiasAprobadas;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return List.of();
    }
}

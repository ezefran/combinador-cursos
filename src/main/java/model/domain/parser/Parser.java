package model.domain.parser;

import model.domain.combinacion.curso.Materia;

import java.io.File;
import java.util.List;

public interface Parser {
    List<Materia> extraerListaMaterias(File archivoMaterias);
    List<Materia> extraerMateriasOfertadas(File archivoMateriasOfertadas);
}

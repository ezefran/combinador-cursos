package model.domain.parser;

public interface ParserFactory {

    static Parser getParser(ParametrosParser parametrosParser) {
        switch (parametrosParser.getUniversidad()) {
            case UNLaM:
                switch (parametrosParser.getCuatrimestre()) {
                    case Cuatrimestre1_2018:
                        switch (parametrosParser.getCarrera()) {
                            case Ingenieria_Informatica:
                                return new ParserUNLaMIngInformatica();
                        }
                        throw new IllegalArgumentException("No hay parser para esta carrera");
                }
                throw new IllegalArgumentException("No hay parser para este cuatrimestre");
        }
        throw new IllegalArgumentException("No hay parser para esta universidad");
    }
}

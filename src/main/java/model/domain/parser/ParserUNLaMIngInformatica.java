package model.domain.parser;

import model.domain.combinacion.curso.Curso;
import model.domain.combinacion.curso.Materia;
import model.utils.Conversor;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.annotation.CheckForNull;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ParserUNLaMIngInformatica implements Parser {

    private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    @Override
    public List<Materia> extraerListaMaterias(File archivoMaterias) {
        List<Materia> materias = new ArrayList<>();
        try {
            Document doc = Jsoup.parse(archivoMaterias, "ISO-8859-15", "");
            Elements tableRows = doc.select("tbody > tr");
            for (Element tableRow : tableRows) {
                if (tableRow.childNodeSize() < 4) continue;
                Materia materia = extraerMateria(tableRow.child(0), tableRow.child(1), tableRow.child(2));
                if (materia != null) {
                    materias.add(materia);
                } else {
                    LOGGER.warning("Materia es null: " + tableRow.text());
                }
            }
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "No se pudo extraer la lista de materias", e);
        }
        return materias;
    }

    @Override
    public List<Materia> extraerMateriasOfertadas(File archivoMateriasOfertadas) {
        List<Materia> materias = new ArrayList<>();
        try {
            Materia materia = null;
            Document doc = Jsoup.parse(archivoMateriasOfertadas, "ISO-8859-15", "");
            Elements tableRows = doc.select("tbody > tr");
            for (Element row : tableRows) {
                Elements columns = row.select("td");
                if (!columns.get(2).text().contains("No Ofertada")) {
                    if (!columns.get(0).toString().contains("&nbsp;")) {
                        // Si es una materia
                        materia = new Materia(
                                Integer.parseInt(limpiarHTML(columns.get(0).text())),
                                limpiarHTML(columns.get(1).text())
                        );
                        materias.add(materia);
                    }
                    if (!columns.get(2).text().contains("No Ofertada")) {
                        if (materia != null) {
                            Curso curso = new Curso(
                                    Integer.parseInt(limpiarHTML(columns.get(2).text())),
                                    Conversor.strToHorarios(limpiarHTML(columns.get(4).text())),
                                    Conversor.strToTipoCurso(limpiarHTML(columns.get(4).text()))
                            );
                            materia.addCurso(curso);
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return materias;
    }

    @CheckForNull
    private Materia extraerMateria(Element codigoMateriaElement, Element nombreMateriaElement, Element correlatividadesElement) {
        String nombreMateria = extraerNombreMateria(nombreMateriaElement);
        if (nombreMateria == null) return null;
        int codigoMateria = extraerCodigoMateria(codigoMateriaElement);
        List<Integer> correlatividad = extraerCorrelatividades(correlatividadesElement);
        return new Materia(codigoMateria, nombreMateria, correlatividad);
    }

    @CheckForNull
    private String extraerNombreMateria(Element nombreMateriaElement) {
        String nombreMateria;
        if (nombreMateriaElement.children().isEmpty()) {
            nombreMateria = nombreMateriaElement.text();
        } else {
            nombreMateria = nombreMateriaElement.child(0).text();
        }

        if (!nombreMateria.contains("Asignatura") && !nombreMateria.contains("Electiva")) {
            return limpiarHTML(nombreMateria.toUpperCase());
        }
        return null;
    }

    private int extraerCodigoMateria(Element codigoMateriaElement) {
        if (!codigoMateriaElement.text().isEmpty()) {
            return Integer.parseInt(limpiarHTML("0"));
        } else {
            return 0;
        }
    }

    private List<Integer> extraerCorrelatividades(Element correlatividadesElement) {
        String correlatividadesStr = limpiarHTML(correlatividadesElement.text());
        String[] correlatividadesArr = correlatividadesStr.split("/");
        List<Integer> correlatividades = new ArrayList<>();
        for (String correlatividad : correlatividadesArr) {
            correlatividad = correlatividad.trim();
            if (correlatividad.contains("Desde")
                    || correlatividad.contains("---")) {
                return null;
            }
            correlatividades.add(Integer.parseInt(correlatividad));
        }
        return correlatividades;
    }

    private static String limpiarHTML(String elemento) {
        return elemento.replaceAll("&nbsp;", "")
                .replaceAll("&#160;", "")
                .replaceAll("\\(.*\\)", "")
                .replaceAll("Á", "A").replaceAll("É", "E").replaceAll("Í", "I")
                .replaceAll("Ó", "O").replaceAll("Ú", "U")
                .trim();
    }
}

package model.utils;

import model.domain.combinacion.curso.TipoCurso;
import model.domain.combinacion.curso.horario.Dia;
import model.domain.combinacion.curso.horario.Hora;
import model.domain.combinacion.curso.horario.Horario;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Conversor {

    public static List<Horario> strToHorarios(String horariosStr) {
        var horarios = new ArrayList<Horario>();
        Pattern pattern = Pattern.compile("([a-zA-Z]+\\d\\da\\d\\d)([a-zA-Z]+\\d\\da\\d\\d)?");
        Matcher matcher = pattern.matcher(horariosStr);
        if (!matcher.lookingAt()) return horarios;

        for (int i = 1; i <= matcher.groupCount(); ++i) {
            var match = matcher.group(i);
            if (match != null) {
                for (var dia : Dia.values()) {
                    var diaAbreviado = dia.name().substring(0, 2);
                    if (match.contains(diaAbreviado)) {
                        int hora = 0;
                        for (int horaPos = 0; horaPos < Hora.horariosArr.length; horaPos++) {
                            if (match.contains(Hora.horariosStrArr[horaPos])) {
                                hora |= Hora.horariosArr[horaPos];
                            }
                        }
                        horarios.add(new Horario(dia, hora));
                    }
                }
            }
        }
        return horarios;
    }

    public static String horariosToString(List<Horario> horarios) {
        var horariosMap = new HashMap<Integer, List<Dia>>();
        for (var horario : horarios) {
            if (!horariosMap.containsKey(horario.getHoras())) {
                horariosMap.put(horario.getHoras(), new ArrayList<>());
            }
            horariosMap.get(horario.getHoras()).add(horario.getDia());
        }

        StringBuilder horariosStr = new StringBuilder();
        for (var hora : horariosMap.keySet()) {
            for (var dia : horariosMap.get(hora)) {
                horariosStr.append(diaToString(dia));
            }
            horariosStr.append(horasToString(hora));
        }

        return horariosStr.toString();
    }

    public static String diaToString(Dia dia) {
        return dia.name().substring(0, 2);
    }

    public static String horasToString(int horas) {
        var horaStr = "";
        for (int horaPos = 0; horaPos < Hora.horariosArr.length; horaPos++) {
            if (horas == Hora.horariosArr[horaPos]) {
                horaStr = Hora.horariosStrArr[horaPos];
            }
        }
        return horaStr;
    }

    public static TipoCurso strToTipoCurso(String tipoCursoStr) {
        tipoCursoStr = tipoCursoStr.toLowerCase();
        if (tipoCursoStr.contains("semi")) {
            return TipoCurso.SEMI_PRESENCIAL;
        } else if (tipoCursoStr.contains("distancia")) {
            return TipoCurso.A_DISTANCIA;
        } else {
            return TipoCurso.PRESENCIAL;
        }
    }

    public static String tipoCursoToString(TipoCurso tipo) {
        switch (tipo) {
            case PRESENCIAL:
                return "Presencial";
            case SEMI_PRESENCIAL:
                return "Semi presencial";
            case A_DISTANCIA:
                return "A distancia";
            default:
                throw new IllegalArgumentException(String.format("El tipo <%s> es invalido", tipo.name()));
        }
    }
}

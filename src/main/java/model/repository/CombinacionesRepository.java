package model.repository;

import model.domain.combinacion.curso.Curso;

import java.util.List;

public interface CombinacionesRepository {
    List<Curso> getCursosCombinacion();
}

package model.repository;

import model.domain.combinacion.curso.Curso;

import java.util.ArrayList;
import java.util.List;

public class DefaultCombinacionesRepository implements CombinacionesRepository {

    private final List<Curso> cursosCombinacion;

    public DefaultCombinacionesRepository() {
        cursosCombinacion = new ArrayList<>();
    }

    @Override
    public List<Curso> getCursosCombinacion() {
        return cursosCombinacion;
    }
}

package model.repository;

import model.domain.combinacion.curso.Curso;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class DefaultCursosRepository implements CursosRepository {

    private final List<Curso> cursosSeleccionados;
    private List<Curso> cursosDisponibles;
    private List<Curso> cursosFiltrados;

    public DefaultCursosRepository() {
        cursosSeleccionados = new ArrayList<>();
        cursosDisponibles = new ArrayList<>();
        cursosFiltrados = new ArrayList<>();
    }

    @Override
    public List<Curso> getCursosSeleccionados() {
        return cursosSeleccionados;
    }

    @Override
    public List<Curso> getCursosDisponibles() {
        cursosDisponibles.sort(Comparator.comparingInt(curso -> curso.getMateria().getCodigo()));
        return cursosDisponibles;
    }

    @Override
    public void setCursosDisponibles(List<Curso> cursos) {
        cursosDisponibles = cursos;
    }

    @Override
    public List<Curso> getCursosFiltrados() {
        return cursosFiltrados;
    }
}

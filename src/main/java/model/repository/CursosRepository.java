package model.repository;

import model.domain.combinacion.curso.Curso;

import java.util.List;

public interface CursosRepository {
    List<Curso> getCursosSeleccionados();
    List<Curso> getCursosDisponibles();
    void setCursosDisponibles(List<Curso> cursos);
    List<Curso> getCursosFiltrados();
}

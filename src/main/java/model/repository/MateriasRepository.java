package model.repository;

import model.domain.combinacion.curso.Materia;

import java.util.List;

public interface MateriasRepository {
    List<Materia> getListaMaterias();
    void setListaMaterias(List<Materia> listaMaterias);
    List<Materia> getMateriasOfertadas();
    void setMateriasOfertadas(List<Materia> materiasOfertadas);
    List<Materia> getMateriasAprobadas();
    void setMateriasAprobadas(List<Materia> materiasAprobadas);
}

package model.repository;

import model.domain.combinacion.curso.Materia;

import java.util.ArrayList;
import java.util.List;

public class DefaultMateriasRepository implements MateriasRepository {

    private List<Materia> listaMaterias;
    private List<Materia> materiasOfertadas;
    private List<Materia> materiasAprobadas;

    public DefaultMateriasRepository() {
        listaMaterias = new ArrayList<>();
        materiasOfertadas = new ArrayList<>();
        materiasAprobadas = new ArrayList<>();
    }

    @Override
    public List<Materia> getListaMaterias() {
        return listaMaterias;
    }

    @Override
    public void setListaMaterias(List<Materia> listaMaterias) {
        this.listaMaterias = listaMaterias;
    }

    @Override
    public List<Materia> getMateriasOfertadas() {
        return materiasOfertadas;
    }

    @Override
    public void setMateriasOfertadas(List<Materia> materiasOfertadas) {
        this.materiasOfertadas = materiasOfertadas;
    }

    @Override
    public List<Materia> getMateriasAprobadas() {
        return materiasAprobadas;
    }

    @Override
    public void setMateriasAprobadas(List<Materia> materiasAprobadas) {
        this.materiasAprobadas = materiasAprobadas;
    }
}

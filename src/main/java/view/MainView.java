package view;

public interface MainView extends View {
    void onGoToCargarArchivos();
    void onGoToSeleccionarMaterias();
    void onGoToFiltrarCursos();
    void onGoToVerCombinaciones();
    void onSubirMateriasAprobadas();
    void onBajarMateriasAprobadas();
    void onClickFiltro();
    void onSalir();

    void abrirTabArchivos();
    void cerrarTabArchivos();

    void abrirTabMaterias();
    void cerrarTabMaterias();

    void abrirTabFiltrarCursos();
    void cerrarTabFiltrarCursos();

    void abrirTabCombinaciones();
    void cerrarTabCombinaciones();

    void reiniciarFiltroCursos();

    void habilitarItemSeleccionarMaterias();
    void deshabilitarItemSeleccionarMaterias();
    void habilitarItemFiltrarCursos();
    void deshabilitarItemFiltrarCursos();
    void habilitarItemCombinaciones();
    void deshabilitarItemCombinaciones();
}

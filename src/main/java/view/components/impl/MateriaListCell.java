package view.components.impl;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import model.domain.combinacion.curso.Materia;
import view.MainView;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class MateriaListCell extends ListCell<Materia> implements Initializable {

    private FXMLLoader loader;

    private final MainView mainView;
    private final List<Materia> materiasAprobadas;

    @FXML
    private VBox root;

    @FXML
    private Label codigo;
    @FXML
    private Label nombre;
    @FXML
    private CheckBox checkBox;

    public MateriaListCell(MainView mainView, List<Materia> materiasAprobadas) {
        this.mainView = mainView;
        this.materiasAprobadas = materiasAprobadas;
    }

    @FXML
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        setOnMouseClicked(e -> {
            checkBox.setSelected(!checkBox.isSelected());
            actualizarMateriasAprobadas();
        });
        checkBox.addEventHandler(MouseEvent.MOUSE_CLICKED, e -> actualizarMateriasAprobadas());
    }

    private void actualizarMateriasAprobadas() {
        if (!materiasAprobadas.contains(getItem())) {
            materiasAprobadas.add(getItem());
        } else {
            materiasAprobadas.remove(getItem());
        }
        mainView.deshabilitarItemFiltrarCursos();
    }

    public boolean chkBoxIsSelected() {
        return checkBox.isSelected();
    }

    @Override
    protected void updateItem(Materia item, boolean empty) {
        super.updateItem(item, empty);

        if (empty || item == null) {
            setGraphic(null);
        } else {
            if (loader == null) {
                loader = new FXMLLoader(getClass().getResource("/views/custom_component/materiaListCell.fxml"));
                loader.setController(this);
                try {
                    loader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            codigo.setText(String.valueOf(item.getCodigo()));
            nombre.setText(item.getNombre());

            if (materiasAprobadas.contains(getItem())) {
                checkBox.setSelected(true);
            } else {
                checkBox.setSelected(false);
            }

            setText(null);
            setGraphic(root);
        }
    }
}

package view.components.impl;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import model.domain.combinacion.Combinacion;
import model.domain.combinacion.curso.Curso;
import model.service.ColoresService;
import presenter.utils.UtilsFx;
import view.components.CombinacionListCellView;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Consumer;

public class CombinacionListCell extends ListCell<Combinacion> implements Initializable, CombinacionListCellView {

    private final GridPane tabla;
    private final Node[][] nodosTabla;
    private final ColoresService coloresService;
    private Consumer<List<Curso>> consumer;

    @FXML
    private VBox root;
    @FXML
    private Label nombre;

    private FXMLLoader loader;

    public CombinacionListCell(ColoresService coloresService, GridPane tabla, Node[][] nodosTabla, Consumer<List<Curso>> consumerCursosCombinacion) {
        this.coloresService = coloresService;
        this.tabla = tabla;
        this.nodosTabla = nodosTabla;
        this.consumer = consumerCursosCombinacion;
    }

    @FXML
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        addEventHandler(MouseEvent.MOUSE_CLICKED, e -> {
            UtilsFx.reiniciarTabla(tabla, nodosTabla);
            consumer.accept(getItem().getCursos());
            UtilsFx.agregarCursosEnTabla(coloresService, tabla, nodosTabla, getItem().getCursos());
        });
    }

    @Override
    protected void updateItem(Combinacion item, boolean empty) {
        super.updateItem(item, empty);

        if (empty || item == null) {
            setGraphic(null);
        } else {
            if (loader == null) {
                loader = new FXMLLoader(getClass().getResource("/views/custom_component/combinacionListCell.fxml"));
                loader.setController(this);
                try {
                    loader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            nombre.setText(getItem().toString());

            setText(null);
            setGraphic(root);
        }
    }
}

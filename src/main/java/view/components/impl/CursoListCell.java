package view.components.impl;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.VBox;
import model.domain.combinacion.curso.Curso;
import model.service.ColoresService;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class CursoListCell extends ListCell<Curso> implements Initializable {

    private final ColoresService coloresService;

    @FXML
    private VBox root;
    @FXML
    private Label codigo;
    @FXML
    private Label codigoMateria;
    @FXML
    private Label nombreMateria;
    @FXML
    private Label horario;
    @FXML
    private Label tipoCurso;
    private FXMLLoader loader;

    public CursoListCell(ColoresService coloresService) {
        this.coloresService = coloresService;
    }

    @FXML
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }

    @Override
    protected void updateItem(Curso item, boolean empty) {
        super.updateItem(item, empty);

        if (empty || item == null) {
            setGraphic(null);
        } else {
            if (loader == null) {
                loader = new FXMLLoader(getClass().getResource("/views/custom_component/cursoListCell.fxml"));
                loader.setController(this);
                try {
                    loader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            var color = coloresService.obtenerColorAsignado(getItem());
            root.getChildren().get(0).setStyle(null);
            root.getChildren().get(0).setStyle("-fx-background-color: " + color);

            codigo.setText(String.valueOf(item.getCodigo()));
            codigoMateria.setText(String.valueOf(item.getMateria().getCodigo()));
            nombreMateria.setText("(" + item.getMateria().getSigla() + ") " + item.getMateria().getNombre());
            horario.setText(item.getHorariosAsString());
            tipoCurso.setText(item.getTipoCursoAsString());

            setText(null);
            setGraphic(root);
        }
    }
}

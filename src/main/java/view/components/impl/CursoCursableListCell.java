package view.components.impl;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import model.domain.combinacion.curso.Curso;
import presenter.TabFiltroCursosPresenter;
import view.MainView;
import view.components.CursoCursableListCellView;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class CursoCursableListCell extends ListCell<Curso> implements Initializable, CursoCursableListCellView {

    private final MainView mainView;
    private final TabFiltroCursosPresenter tabFiltroCursosPresenter;

    @FXML
    private VBox root;
    @FXML
    private Label codigo;
    @FXML
    private Label codigoMateria;
    @FXML
    private Label nombreMateria;
    @FXML
    private Label horario;
    @FXML
    private Label tipoCurso;
    @FXML
    private CheckBox checkBox;

    private FXMLLoader loader;

    public CursoCursableListCell(MainView mainView, TabFiltroCursosPresenter tabFiltroCursosPresenter) {
        this.mainView = mainView;
        this.tabFiltroCursosPresenter = tabFiltroCursosPresenter;
    }

    @FXML
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        setOnMouseClicked(e -> {
            checkBox.setSelected(!checkBox.isSelected());
            tabFiltroCursosPresenter.onClickCurso(mainView, tabFiltroCursosPresenter, getItem());
        });
        checkBox.addEventHandler(MouseEvent.MOUSE_CLICKED, e -> tabFiltroCursosPresenter.onClickCurso(mainView, tabFiltroCursosPresenter, getItem()));
    }

    public boolean chkBoxIsSelected() {
        return checkBox.isSelected();
    }

    @Override
    protected void updateItem(Curso item, boolean empty) {
        super.updateItem(item, empty);

        if (empty || item == null) {
            setGraphic(null);
        } else {
            if (loader == null) {
                loader = new FXMLLoader(getClass().getResource("/views/custom_component/cursoCursableListCell.fxml"));
                loader.setController(this);
                try {
                    loader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            codigo.setText(String.valueOf(item.getCodigo()));
            codigoMateria.setText(String.valueOf(item.getMateria().getCodigo()));
            nombreMateria.setText(item.getMateria().getNombre());
            horario.setText(item.getHorariosAsString());
            tipoCurso.setText(item.getTipoCursoAsString());
            setText(null);
            setGraphic(root);

            tabFiltroCursosPresenter.onUpdateItem(this, getItem());
        }
    }

    @Override
    public void seleccionar(boolean seleccionado) {
        checkBox.setSelected(seleccionado);
    }
}

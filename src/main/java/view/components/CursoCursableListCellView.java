package view.components;

import view.View;

public interface CursoCursableListCellView extends View {
    void seleccionar(boolean seleccionado);
}

package view.constants;

public class Textos {

    private Textos() {}

    public static final String STR_CARGAR_ARCHIVOS = "CARGAR ARCHIVOS";
    public static final String STR_SELECCIONAR_MATERIAS = "SELECCIONAR MATERIAS";
    public static final String STR_SELECCIONAR_CURSOS = "SELECCIONAR CURSOS";
    public static final String STR_COMBINACIONES = "COMBINACIONES";
}

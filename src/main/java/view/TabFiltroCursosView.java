package view;

import model.domain.combinacion.curso.Curso;

import java.util.List;

public interface TabFiltroCursosView extends View {
    void mostrarCursos(List<Curso> cursos);

    void refrescarListaCursos();
}

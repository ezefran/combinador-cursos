package view;

import model.domain.combinacion.Combinacion;
import model.domain.combinacion.curso.Curso;
import model.service.ColoresService;

import java.util.List;

public interface TabCombinacionesView extends View {
    void iniciarListaCombinaciones(ColoresService coloresService, List<Combinacion> combinaciones);
    void iniciarListaCursosCombinacion(ColoresService coloresService, List<Curso> cursosCombinacion);
}

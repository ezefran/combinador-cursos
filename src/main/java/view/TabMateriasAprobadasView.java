package view;

import model.domain.combinacion.curso.Materia;

import java.util.List;

public interface TabMateriasAprobadasView extends View {
    void mostrarMaterias(List<Materia> listaMaterias, List<Materia> materiasAprobadas);
}

package view;

public interface TabArchivosView extends View {
    void onSeleccionarUniversidad();
    void onSeleccionarCarrera();
    void onSeleccionarCuatrimestre();
    void onSubirLista();
    void onSubirOferta();

    void onCargarArchivos();
}

package view.impl;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import model.domain.combinacion.curso.Materia;
import presenter.TabMateriasAprobadasPresenter;
import view.MainView;
import view.TabMateriasAprobadasView;
import view.components.impl.MateriaListCell;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class DefaultTabMateriasAprobadasView implements TabMateriasAprobadasView, Initializable {

    private final MainView mainView;
    private final TabMateriasAprobadasPresenter tabMateriasAprobadasPresenter;

    @FXML
    private AnchorPane root;

    @FXML
    private ListView<Materia> listaMateriasView;
    @FXML
    private Button btnAceptarMaterias;

    public DefaultTabMateriasAprobadasView(MainView mainView, TabMateriasAprobadasPresenter tabMateriasAprobadasPresenter) {
        this.mainView = mainView;
        this.tabMateriasAprobadasPresenter = tabMateriasAprobadasPresenter;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        tabMateriasAprobadasPresenter.iniciar(this);
    }

    @Override
    public void mostrarMaterias(List<Materia> listaMaterias, List<Materia> materiasAprobadas) {
        listaMateriasView.setCellFactory(l -> new MateriaListCell(mainView, materiasAprobadas));
        var listaMateriasObs = FXCollections.observableList(listaMaterias);
        listaMateriasView.setItems(listaMateriasObs);
    }
}

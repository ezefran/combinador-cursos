package view.impl;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import model.domain.combinacion.curso.Curso;
import presenter.TabFiltroCursosPresenter;
import view.MainView;
import view.TabFiltroCursosView;
import view.components.impl.CursoCursableListCell;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class DefaultTabFiltroCursosView implements TabFiltroCursosView, Initializable {

    private final MainView mainView;
    private final TabFiltroCursosPresenter tabFiltroCursosPresenter;

    @FXML
    private AnchorPane root;

    @FXML
    private ListView<Curso> listaCursables;

    public DefaultTabFiltroCursosView(MainView mainView, TabFiltroCursosPresenter tabFiltroCursosPresenter) {
        this.mainView = mainView;
        this.tabFiltroCursosPresenter = tabFiltroCursosPresenter;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        tabFiltroCursosPresenter.iniciar(this);
    }

    @Override
    public void mostrarCursos(List<Curso> cursos) {
        listaCursables.setCellFactory(l -> new CursoCursableListCell(mainView, tabFiltroCursosPresenter));
        var listaCursosObs = FXCollections.observableList(cursos);
        listaCursables.setItems(listaCursosObs);
    }

    @Override
    public void refrescarListaCursos() {
        listaCursables.refresh();
    }
}

package view.impl;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import model.domain.combinacion.Combinacion;
import model.domain.combinacion.curso.Curso;
import model.service.ColoresService;
import presenter.impl.DefaultTabCombinacionesPresenter;
import presenter.utils.UtilsFx;
import view.TabCombinacionesView;
import view.components.impl.CombinacionListCell;
import view.components.impl.CursoListCell;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Consumer;

public class DefaultTabCombinacionesView implements TabCombinacionesView, Initializable {

    private final DefaultTabCombinacionesPresenter tabCombinacionesPresenter;

    private Node[][] nodosTabla;

    @FXML
    private VBox root;

    @FXML
    private ListView<Combinacion> listaCombinaciones;
    @FXML
    private ListView<Curso> listaCursosCombinacion;
    @FXML
    private GridPane tablaSemana;

    public DefaultTabCombinacionesView(DefaultTabCombinacionesPresenter tabCombinacionesPresenter) {
        this.tabCombinacionesPresenter = tabCombinacionesPresenter;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        nodosTabla = new Node[6][7];
        UtilsFx.reiniciarTabla(tablaSemana, nodosTabla);
        tabCombinacionesPresenter.iniciar(this);
    }

    @Override
    public void iniciarListaCombinaciones(ColoresService coloresService, List<Combinacion> combinaciones) {
        Consumer<List<Curso>> consumerCursosCombinacion = cursosCombinacion -> listaCursosCombinacion.setItems(FXCollections.observableList(cursosCombinacion));
        listaCombinaciones.setCellFactory(l -> new CombinacionListCell(coloresService, tablaSemana, nodosTabla, consumerCursosCombinacion));
        listaCombinaciones.setItems(FXCollections.observableList(combinaciones));
    }

    @Override
    public void iniciarListaCursosCombinacion(ColoresService coloresService, List<Curso> cursosCombinacion) {
        listaCursosCombinacion.setCellFactory(l -> new CursoListCell(coloresService));
        listaCursosCombinacion.setOnMouseClicked(e -> {
        });
    }
}

package view.impl;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import model.domain.Carrera;
import model.domain.Cuatrimestre;
import model.domain.Universidad;
import presenter.MainPresenter;
import presenter.impl.DefaultTabArchivosPresenter;
import presenter.utils.UtilsFx;
import view.MainView;
import view.TabArchivosView;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

public class DefaultTabArchivosView implements TabArchivosView, Initializable {

    private final MainView mainView;
    private final MainPresenter mainPresenter;
    private final DefaultTabArchivosPresenter tabArchivosPresenter;

    private boolean hayUniversidad;
    private boolean hayCarrera;
    private boolean hayCuatrimestre;

    @FXML
    private VBox root;

    @FXML
    private ComboBox<Universidad> boxUniversidad;
    @FXML
    private ComboBox<Carrera> boxCarrera;
    @FXML
    private ComboBox<Cuatrimestre> boxCuatrimestre;
    @FXML
    private TextField txtLista;
    @FXML
    private TextField txtOferta;
    @FXML
    private Button btnSubirLista;
    @FXML
    private Button btnSubirOferta;
    @FXML
    private Button btnCargarMaterias;

    public DefaultTabArchivosView(MainPresenter mainPresenter, MainView mainView, DefaultTabArchivosPresenter tabArchivosPresenter) {
        this.mainPresenter = mainPresenter;
        this.mainView = mainView;
        this.tabArchivosPresenter = tabArchivosPresenter;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        tabArchivosPresenter.iniciar();
        boxUniversidad.setItems(FXCollections.observableArrayList(Universidad.values()));
        boxCarrera.setItems(FXCollections.observableArrayList(Carrera.values()));
        boxCuatrimestre.setItems(FXCollections.observableArrayList(Cuatrimestre.values()));
    }

    @FXML
    @Override
    public void onSeleccionarUniversidad() {
        onCambiosOpcionesCargarArchivos();
        hayUniversidad = boxUniversidad.getSelectionModel().getSelectedIndex() != -1;
    }

    @FXML
    @Override
    public void onSeleccionarCarrera() {
        onCambiosOpcionesCargarArchivos();
        hayCarrera = boxCarrera.getSelectionModel().getSelectedIndex() != -1;
    }

    @FXML
    @Override
    public void onSeleccionarCuatrimestre() {
        onCambiosOpcionesCargarArchivos();
        hayCuatrimestre = boxCuatrimestre.getSelectionModel().getSelectedIndex() != -1;
    }

    @FXML
    @Override
    public void onSubirLista() {
        var f = UtilsFx.abrirArchivo(root, "*.html");
        if (f != null) {
            onCambiosOpcionesCargarArchivos();
            txtLista.setText(f.getAbsolutePath());
        }
    }

    @FXML
    @Override
    public void onSubirOferta() {
        var f = UtilsFx.abrirArchivo(root, "*.html");
        if (f != null) {
            onCambiosOpcionesCargarArchivos();
            txtOferta.setText(f.getAbsolutePath());
        }
    }

    private void onCambiosOpcionesCargarArchivos() {
        mainPresenter.onCambiosOpcionesCargarArchivos(mainView);
    }

    @Override
    public void onCargarArchivos() {
        if (hayUniversidad && hayCarrera && hayCuatrimestre && !txtLista.getText().equals("") && !txtOferta.getText().equals("")) {
            File archivoListaMaterias = new File(txtLista.getText());
            File archivoOfertaMaterias = new File(txtOferta.getText());
            Universidad universidad = boxUniversidad.getSelectionModel().getSelectedItem();
            Carrera carrera = boxCarrera.getSelectionModel().getSelectedItem();
            Cuatrimestre cuatrimestre = boxCuatrimestre.getSelectionModel().getSelectedItem();
            tabArchivosPresenter.onCargarArchivos(mainPresenter, mainView, archivoListaMaterias, archivoOfertaMaterias, universidad, carrera, cuatrimestre);
        } else if (!hayCarrera || !hayUniversidad || !hayCuatrimestre || txtLista.getText().equals("") || txtOferta.getText().equals("")) {
            UtilsFx.alertaInfo("Cargar materias", "Falta ingresar datos");
        }
    }
}

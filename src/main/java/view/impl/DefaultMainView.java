package view.impl;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import model.domain.combinacion.curso.TipoCurso;
import model.domain.combinacion.curso.horario.Dia;
import model.domain.combinacion.curso.horario.Hora;
import presenter.MainPresenter;
import presenter.impl.DefaultTabArchivosPresenter;
import presenter.impl.DefaultTabCombinacionesPresenter;
import presenter.impl.DefaultTabFiltroCursosPresenter;
import presenter.impl.DefaultTabMateriasAprobadasPresenter;
import presenter.utils.UtilsFx;
import view.*;
import view.constants.Textos;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class DefaultMainView implements MainView, Initializable {

    private final MainPresenter mainPresenter;

    // root
    @FXML
    private BorderPane root;

    // topBar
    @FXML
    private TabPane tabPane;
    @FXML
    private HBox leftTopBar;
    @FXML
    private HBox centerTopBar;
    @FXML
    private HBox rightTopBar;
    @FXML
    private Button btnBuscar;
    @FXML
    private TextField txtBuscarInput;
    @FXML
    private Label txtBarTitle;
    @FXML
    private Button btnSubirMateriasAprobadas;
    @FXML
    private Button btnBajarMateriasAprobadas;

    // botBar
    @FXML
    private HBox bottomBar;
    @FXML
    private Button btnConfirmar;

    // menu
    @FXML
    private MenuItem itemCargarArchivos;
    @FXML
    MenuItem itemSeleccionarMaterias;
    @FXML
    MenuItem itemFiltrarCursos;
    @FXML
    MenuItem itemCombinaciones;

    // menu-filtro
    @FXML
    private MenuButton btnFiltroCursos;
    @FXML
    private CheckMenuItem chkLunes;
    @FXML
    private CheckMenuItem chkMartes;
    @FXML
    private CheckMenuItem chkMiercoles;
    @FXML
    private CheckMenuItem chkJueves;
    @FXML
    private CheckMenuItem chkViernes;
    @FXML
    private CheckMenuItem chkSabado;
    @FXML
    private CheckMenuItem chk08;
    @FXML
    private CheckMenuItem chk10;
    @FXML
    private CheckMenuItem chk12;
    @FXML
    private CheckMenuItem chk14;
    @FXML
    private CheckMenuItem chk16;
    @FXML
    private CheckMenuItem chk19;
    @FXML
    private CheckMenuItem chk21;
    @FXML
    private CheckMenuItem chkPresencial;
    @FXML
    private CheckMenuItem chkSemi;
    @FXML
    private CheckMenuItem chkDistancia;
    @FXML
    private Button btnAceptarCursos;

    private final TabArchivosView tabArchivosView;
    private final TabMateriasAprobadasView tabMateriasAprobadasView;
    private final TabFiltroCursosView tabFiltroCursosView;
    private final TabCombinacionesView tabCombinacionesView;

    public DefaultMainView(
            MainPresenter mainPresenter,
            DefaultTabArchivosPresenter tabArchivosPresenter,
            DefaultTabMateriasAprobadasPresenter tabMateriasAprobadasPresenter,
            DefaultTabFiltroCursosPresenter tabFiltroCursosPresenter,
            DefaultTabCombinacionesPresenter tabCombinacionesPresenter) {
        this.mainPresenter = mainPresenter;

        tabArchivosView = new DefaultTabArchivosView(mainPresenter, this, tabArchivosPresenter);
        tabMateriasAprobadasView = new DefaultTabMateriasAprobadasView(this, tabMateriasAprobadasPresenter);
        tabFiltroCursosView = new DefaultTabFiltroCursosView(this, tabFiltroCursosPresenter);
        tabCombinacionesView = new DefaultTabCombinacionesView(tabCombinacionesPresenter);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        mainPresenter.iniciar(this);

        txtBarTitle.setText(Textos.STR_CARGAR_ARCHIVOS);

        // remover nodos del menu
        leftTopBar.getChildren().remove(btnFiltroCursos);
        leftTopBar.getChildren().remove(btnSubirMateriasAprobadas);
        leftTopBar.getChildren().remove(btnBajarMateriasAprobadas);
        rightTopBar.getChildren().remove(btnBuscar);
        rightTopBar.getChildren().remove(txtBuscarInput);
    }

    private void cargarTab(String nombre, View view) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/tab" + nombre + ".fxml"));
        loader.setController(view);
        try {
            Pane contenidoTabArchivos = loader.load();
            contenidoTabArchivos.getStylesheets().add("/styles/default.css");
            Pane tabArchivos = (Pane) root.lookup("#contenidoTab" + nombre);
            tabArchivos.getChildren().clear();
            tabArchivos.getChildren().add(contenidoTabArchivos);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Menu
    @FXML
    @Override
    public void onGoToCargarArchivos() {
        mainPresenter.onGoToCargarArchivos(this);
    }

    @FXML
    @Override
    public void onGoToSeleccionarMaterias() {
        mainPresenter.onGoToSeleccionarMaterias(this);
    }

    @FXML
    @Override
    public void onGoToFiltrarCursos() {
        mainPresenter.onGoToFiltrarCursos(this);
    }

    @FXML
    @Override
    public void onGoToVerCombinaciones() {
        mainPresenter.onGoToVerCombinaciones(this);
    }

    @FXML
    @Override
    public void onSubirMateriasAprobadas() {
        File archivo = UtilsFx.abrirArchivo(root, "*.json");
        mainPresenter.onSubirMateriasAprobadas(this, archivo);
        itemFiltrarCursos.setDisable(false);
    }

    @FXML
    @Override
    public void onBajarMateriasAprobadas() {
        File archivo = UtilsFx.guardarArchivo(root, "*.json");
        mainPresenter.onBajarMateriasAprobadas(this, archivo);
    }

    // tab-cursos
    @FXML
    @Override
    public void onClickFiltro() {
        var diasActivos = new ArrayList<Dia>();
        int horasActivas = 0;
        var tiposActivos = new ArrayList<TipoCurso>();

        if (chkLunes.isSelected()) {
            diasActivos.add(Dia.Lunes);
        }
        if (chkMartes.isSelected()) {
            diasActivos.add(Dia.Martes);
        }
        if (chkMiercoles.isSelected()) {
            diasActivos.add(Dia.Miercoles);
        }
        if (chkJueves.isSelected()) {
            diasActivos.add(Dia.Jueves);
        }
        if (chkViernes.isSelected()) {
            diasActivos.add(Dia.Viernes);
        }
        if (chkSabado.isSelected()) {
            diasActivos.add(Dia.Sabado);
        }

        if (chk08.isSelected()) {
            horasActivas |= Hora.M08a10;
        }
        if (chk10.isSelected()) {
            horasActivas |= Hora.M10a12;
        }
        if (chk12.isSelected()) {
            horasActivas |= Hora.T12a14;
        }
        if (chk14.isSelected()) {
            horasActivas |= Hora.T14a16;
        }
        if (chk16.isSelected()) {
            horasActivas |= Hora.T16a18;
        }
        if (chk19.isSelected()) {
            horasActivas |= Hora.N19a21;
        }
        if (chk21.isSelected()) {
            horasActivas |= Hora.N21a23;
        }

        if (chkPresencial.isSelected()) {
            tiposActivos.add(TipoCurso.PRESENCIAL);
        }
        if (chkSemi.isSelected()) {
            tiposActivos.add(TipoCurso.SEMI_PRESENCIAL);
        }
        if (chkDistancia.isSelected()) {
            tiposActivos.add(TipoCurso.A_DISTANCIA);
        }

        mainPresenter.onClickFiltro(tabFiltroCursosView, diasActivos, horasActivas, tiposActivos);
    }

    @FXML
    @Override
    public void onSalir() {
        mainPresenter.onSalir(this);
        Platform.exit();
    }

    @Override
    public void abrirTabArchivos() {
        mostrarBottomBar(true);
        txtBarTitle.setText(Textos.STR_CARGAR_ARCHIVOS);
        tabPane.getSelectionModel().select(0);
        btnConfirmar.setOnAction(event -> tabArchivosView.onCargarArchivos());
        cargarTab("Archivos", tabArchivosView);
    }

    @Override
    public void cerrarTabArchivos() {

    }

    @Override
    public void abrirTabMaterias() {
        mostrarBottomBar(true);
        txtBarTitle.setText(Textos.STR_SELECCIONAR_MATERIAS);
        leftTopBar.getChildren().add(btnBajarMateriasAprobadas);
        leftTopBar.getChildren().add(btnSubirMateriasAprobadas);
        tabPane.getSelectionModel().select(1);
        btnConfirmar.setOnAction(event -> mainPresenter.onConfirmarMaterias(this));
        cargarTab("MateriasAprobadas", tabMateriasAprobadasView);
    }

    @Override
    public void cerrarTabMaterias() {
        leftTopBar.getChildren().remove(btnBajarMateriasAprobadas);
        leftTopBar.getChildren().remove(btnSubirMateriasAprobadas);
    }

    @Override
    public void abrirTabFiltrarCursos() {
        mostrarBottomBar(true);
        txtBarTitle.setText(Textos.STR_SELECCIONAR_CURSOS);
        leftTopBar.getChildren().add(btnFiltroCursos);
        tabPane.getSelectionModel().select(2);
        btnConfirmar.setOnAction(event -> mainPresenter.onConfirmarCursosSeleccionados(this));
        reiniciarFiltroCursos();
        cargarTab("FiltroCursos", tabFiltroCursosView);
    }

    @Override
    public void cerrarTabFiltrarCursos() {
        leftTopBar.getChildren().remove(btnFiltroCursos);
    }

    @Override
    public void abrirTabCombinaciones() {
        mostrarBottomBar(false);
        txtBarTitle.setText(Textos.STR_COMBINACIONES);
        tabPane.getSelectionModel().select(3);
        cargarTab("Combinaciones", tabCombinacionesView);
    }

    @Override
    public void cerrarTabCombinaciones() {

    }

    private void mostrarBottomBar(boolean estado) {
        bottomBar.setVisible(estado);
        bottomBar.setManaged(estado);
    }

    @Override
    public void reiniciarFiltroCursos() {
        chkLunes.setSelected(true);
        chkMartes.setSelected(true);
        chkMiercoles.setSelected(true);
        chkJueves.setSelected(true);
        chkViernes.setSelected(true);
        chkSabado.setSelected(true);
        chk08.setSelected(true);
        chk10.setSelected(true);
        chk12.setSelected(true);
        chk14.setSelected(true);
        chk16.setSelected(true);
        chk19.setSelected(true);
        chk21.setSelected(true);
        chkPresencial.setSelected(true);
        chkSemi.setSelected(true);
        chkDistancia.setSelected(true);
    }

    @Override
    public void habilitarItemSeleccionarMaterias() {
        itemSeleccionarMaterias.setDisable(false);
    }

    @Override
    public void deshabilitarItemSeleccionarMaterias() {
        itemSeleccionarMaterias.setDisable(true);
    }

    @Override
    public void habilitarItemFiltrarCursos() {
        itemFiltrarCursos.setDisable(false);
    }

    @Override
    public void deshabilitarItemFiltrarCursos() {
        itemFiltrarCursos.setDisable(true);
    }

    @Override
    public void habilitarItemCombinaciones() {
        itemCombinaciones.setDisable(true);
    }

    @Override
    public void deshabilitarItemCombinaciones() {
        itemCombinaciones.setDisable(true);
    }
}

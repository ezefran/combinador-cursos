import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import model.repository.DefaultCombinacionesRepository;
import model.repository.DefaultCursosRepository;
import model.repository.DefaultMateriasRepository;
import model.service.*;
import presenter.impl.*;
import view.MainView;
import view.impl.DefaultMainView;

import java.io.IOException;

public class CombinadorCursos extends Application {

    public static void main(String[] args) {
        launch(CombinadorCursos.class, args);
    }

    @Override
    public void start(Stage stage) {
        try {
            iniciarGui(stage);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void iniciarGui(Stage stage) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/main.fxml"));
        loader.setController(crearMainView());
        Pane root = loader.load();
        root.getStylesheets().add("/styles/default.css");
        stage.setTitle("Combinador de cursos");
        stage.setScene(new Scene(root));
        stage.setMinWidth(800);
        stage.setMinHeight(600);
        stage.show();
    }

    private MainView crearMainView() {
        var materiasRepository = new DefaultMateriasRepository();
        var cursosRepository = new DefaultCursosRepository();
        var combinacionesRepository = new DefaultCombinacionesRepository();

        var archivosService = new ArchivosService(materiasRepository);
        var materiasService = new MateriasService(materiasRepository);
        var cursosService = new CursosService(cursosRepository);
        var combinacionService = new CombinacionService(combinacionesRepository);
        var filtroCombinacionService = new FiltroCombinacionService(cursosService);
        var coloresService = new ColoresService();

        var mainPresenter = new DefaultMainPresenter(archivosService, filtroCombinacionService);

        var tabArchivosPresenter = new DefaultTabArchivosPresenter(materiasService);
        var tabMateriasAprobadasPresenter = new DefaultTabMateriasAprobadasPresenter(materiasService);
        var tabFiltroCursosPresenter = new DefaultTabFiltroCursosPresenter(materiasService, cursosService);
        var tabCombinacionesPresenter = new DefaultTabCombinacionesPresenter(combinacionService, cursosService, coloresService);

        return new DefaultMainView(
                mainPresenter,
                tabArchivosPresenter,
                tabMateriasAprobadasPresenter,
                tabFiltroCursosPresenter,
                tabCombinacionesPresenter);
    }
}

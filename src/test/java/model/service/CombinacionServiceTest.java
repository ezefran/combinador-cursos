package model.service;

import model.domain.combinacion.Combinacion;
import model.domain.combinacion.curso.Curso;
import model.domain.combinacion.curso.Materia;
import model.domain.combinacion.curso.TipoCurso;
import model.domain.combinacion.curso.horario.Dia;
import model.domain.combinacion.curso.horario.Hora;
import model.domain.combinacion.curso.horario.Horario;
import model.repository.CombinacionesRepository;
import model.repository.DefaultCombinacionesRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class CombinacionServiceTest {

    private static final int DEFAULT_MAX_CURSOS_POR_COMBINACION = 14;

    private CombinacionesRepository combinacionesRepository;
    private CombinacionService combinacionService;

    private boolean setUp;
    private List<Horario> horarios;
    private List<Curso> cursos;
    private List<Combinacion> combinaciones;

    @BeforeEach
    public void setUp() {
        if (setUp) return;

        combinacionesRepository = new DefaultCombinacionesRepository();
        combinacionService = new CombinacionService(combinacionesRepository);

        horarios = new ArrayList<>();
        horarios.add(new Horario(Dia.Lunes, Hora.M08a10 | Hora.M10a12));
        horarios.add(new Horario(Dia.Martes, Hora.M08a10 | Hora.M10a12));
        horarios.add(new Horario(Dia.Miercoles, Hora.M08a10 | Hora.M10a12));

        cursos = new ArrayList<>();
        cursos.add(new Curso(1, horarios.get(0), TipoCurso.PRESENCIAL));
        cursos.add(new Curso(2, horarios.get(1), TipoCurso.PRESENCIAL));
        cursos.add(new Curso(3, horarios.get(2), TipoCurso.PRESENCIAL));
        cursos.add(new Curso(4, horarios.get(0), TipoCurso.PRESENCIAL));
        cursos.add(new Curso(5, horarios.get(1), TipoCurso.PRESENCIAL));

        cursos.get(0).setMateria(new Materia(0, "1"));
        cursos.get(1).setMateria(new Materia(1, "2"));
        cursos.get(2).setMateria(new Materia(2, "3"));
        cursos.get(3).setMateria(new Materia(3, "4"));
        cursos.get(4).setMateria(new Materia(3, "4"));

        combinaciones = new ArrayList<>();
        combinaciones.add(new Combinacion(cursos.get(0)));
        combinaciones.add(new Combinacion(cursos.get(1)));
        combinaciones.add(new Combinacion(cursos.get(2)));

        setUp = true;
    }

    @Test
    public void buscarCombinaciones_3cursos() {
        List<Curso> lCursos = cursos.subList(0, 3);

        var resultado = combinacionService.buscarCombinaciones(lCursos, DEFAULT_MAX_CURSOS_POR_COMBINACION);

        assertThat(resultado).hasSize(7);
    }

    @Test
    public void buscarCombinaciones_3cursosConLimite() {
        List<Curso> lCursos = cursos.subList(0, 3);

        var resultado = combinacionService.buscarCombinaciones(lCursos, 2);

        assertThat(resultado).hasSize(6);
    }

    @Test
    public void buscarCombinaciones_2cursosConMismaMateria() {
        List<Curso> lCursos = cursos.subList(3, 5);

        var resultado = combinacionService.buscarCombinaciones(lCursos, DEFAULT_MAX_CURSOS_POR_COMBINACION);

        assertThat(resultado).hasSize(2);
    }
}

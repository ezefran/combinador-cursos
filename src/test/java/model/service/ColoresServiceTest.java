package model.service;

import model.domain.combinacion.curso.Curso;
import model.domain.combinacion.curso.TipoCurso;
import model.domain.combinacion.curso.horario.Dia;
import model.domain.combinacion.curso.horario.Hora;
import model.domain.combinacion.curso.horario.Horario;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class ColoresServiceTest {

    private static final String PRIMER_COLOR = "#EFAFAF";
    private static final String ULTIMO_COLOR = "#AFEFE6";

    private ColoresService coloresService;

    private boolean setup;
    private List<Curso> cursos;


    @BeforeEach
    public void setup() {
        if (setup) return;

        coloresService = new ColoresService();

        Horario horario = new Horario(Dia.Lunes, Hora.M08a10 | Hora.M10a12);

        cursos = new ArrayList<>();
        cursos.add(new Curso(1, horario, TipoCurso.PRESENCIAL));
        cursos.add(new Curso(2, horario, TipoCurso.PRESENCIAL));
        cursos.add(new Curso(3, horario, TipoCurso.PRESENCIAL));
        cursos.add(new Curso(4, horario, TipoCurso.PRESENCIAL));
        cursos.add(new Curso(5, horario, TipoCurso.PRESENCIAL));
        cursos.add(new Curso(6, horario, TipoCurso.PRESENCIAL));
        cursos.add(new Curso(7, horario, TipoCurso.PRESENCIAL));
        cursos.add(new Curso(8, horario, TipoCurso.PRESENCIAL));
        cursos.add(new Curso(9, horario, TipoCurso.PRESENCIAL));
        cursos.add(new Curso(10, horario, TipoCurso.PRESENCIAL));
        cursos.add(new Curso(11, horario, TipoCurso.PRESENCIAL));
        cursos.add(new Curso(12, horario, TipoCurso.PRESENCIAL));
        cursos.add(new Curso(13, horario, TipoCurso.PRESENCIAL));
        cursos.add(new Curso(14, horario, TipoCurso.PRESENCIAL));
        cursos.add(new Curso(15, horario, TipoCurso.PRESENCIAL));

        setup = true;
    }

    @Test
    public void asignarColor_unSoloColor_retornaPrimerColorAsignado() {
        String color = coloresService.obtenerColorAsignado(cursos.get(0));
        assertThat(color).isEqualTo(PRIMER_COLOR);
    }

    @Test
    public void asignarColor_todosLosColoresMasUno_retornaPrimerYultimoColor() {
        List<String> colores = new ArrayList<>();

        for (Curso curso : cursos) {
            colores.add(coloresService.obtenerColorAsignado(curso));
        }

        assertThat(colores.get(13)).isEqualTo(ULTIMO_COLOR);
        assertThat(colores.get(14)).isEqualTo(PRIMER_COLOR);
    }
}

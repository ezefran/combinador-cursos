package model.service;

import model.domain.combinacion.curso.Curso;
import model.domain.combinacion.curso.Materia;
import model.domain.combinacion.curso.TipoCurso;
import model.domain.combinacion.curso.horario.Dia;
import model.domain.combinacion.curso.horario.Hora;
import model.domain.combinacion.curso.horario.Horario;
import model.repository.CursosRepository;
import model.repository.DefaultCursosRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class FiltroCombinacionServiceTest {

    private CursosRepository cursosRepository;
    private CursosService cursosService;
    private FiltroCombinacionService filtroCombinacionService;

    private List<Curso> cursos;
    private List<Horario> horarios;

    @BeforeEach
    public void setup() {
        cursosRepository = new DefaultCursosRepository();
        cursosService = new CursosService(cursosRepository);
        filtroCombinacionService = new FiltroCombinacionService(cursosService);

        horarios = new ArrayList<>();
        horarios.add(new Horario(Dia.Lunes, Hora.M08a10 | Hora.M10a12));
        horarios.add(new Horario(Dia.Martes, Hora.T12a14 | Hora.T14a16));
        horarios.add(new Horario(Dia.Miercoles, Hora.N19a21 | Hora.N21a23));

        cursos = new ArrayList<>();
        cursos.add(new Curso(1, horarios.get(0), TipoCurso.PRESENCIAL));
        cursos.add(new Curso(2, horarios.get(1), TipoCurso.PRESENCIAL));
        cursos.add(new Curso(3, horarios.get(2), TipoCurso.PRESENCIAL));
        cursos.add(new Curso(4, horarios.get(0), TipoCurso.PRESENCIAL));
        cursos.add(new Curso(5, horarios.get(1), TipoCurso.PRESENCIAL));

        cursos.get(0).setMateria(new Materia(0, "1"));
        cursos.get(1).setMateria(new Materia(1, "2"));
        cursos.get(2).setMateria(new Materia(2, "3"));
        cursos.get(3).setMateria(new Materia(3, "4"));
        cursos.get(4).setMateria(new Materia(3, "4"));

        cursosRepository.getCursosDisponibles().addAll(cursos);
    }

    @Test
    public void filtrarCursosCursables_sinParametros_retornaNinguno() {
        filtroCombinacionService.filtrarCursosCursables(List.of(), 0, List.of());

        assertThat(cursosService.buscarCursosSeleccionados()).isEmpty();
    }

    @Test
    public void filtrarCursosCursables_todosLosParametros_retornaTodos() {
        List<Curso> cursosSeleccionados = new ArrayList<>();
        cursos.forEach(curso -> seleccionarCurso(curso, cursosSeleccionados));

        filtroCombinacionService.filtrarCursosCursables(
                List.of(Dia.values()),
                Hora.M08a12 | Hora.T12a14 | Hora.T14a18 | Hora.N19a23,
                List.of(TipoCurso.values()));

        assertThat(cursosSeleccionados).containsExactlyInAnyOrderElementsOf(cursos);
    }

    @Test
    public void filtrarCursosCursables_filtraTodoMenosLunes() {
        List<Curso> cursosSeleccionados = new ArrayList<>();
        var cursosEsperados = List.of(cursos.get(1), cursos.get(2), cursos.get(4));
        cursosEsperados.forEach(curso -> seleccionarCurso(curso, cursosSeleccionados));

        filtroCombinacionService.filtrarCursosCursables(
                List.of(Dia.Martes, Dia.Miercoles, Dia.Jueves, Dia.Viernes, Dia.Sabado),
                Hora.M08a12 | Hora.T12a14 | Hora.T14a18 | Hora.N19a23,
                List.of(TipoCurso.values()));

        assertThat(cursosSeleccionados).containsExactlyInAnyOrderElementsOf(cursosEsperados);
    }

    @Test
    public void filtrarCursosCursables_filtraTodoMenosTarde() {
        List<Curso> cursosSeleccionados = new ArrayList<>();
        var cursosEsperados = List.of(cursos.get(0), cursos.get(2), cursos.get(3));
        cursosEsperados.forEach(curso -> seleccionarCurso(curso, cursosSeleccionados));

        filtroCombinacionService.filtrarCursosCursables(
                List.of(Dia.Martes, Dia.Miercoles, Dia.Jueves, Dia.Viernes, Dia.Sabado),
                Hora.M08a12 | Hora.T12a14 | Hora.T14a18 | Hora.N19a23,
                List.of(TipoCurso.values()));

        assertThat(cursosSeleccionados).containsExactlyInAnyOrderElementsOf(cursosEsperados);
    }

    private void seleccionarCurso(Curso curso, List<Curso> cursosSeleccionados) {
        if (!cursosSeleccionados.contains(curso)) {
            cursosSeleccionados.add(curso);
        } else {
            cursosSeleccionados.remove(curso);
        }
    }
}

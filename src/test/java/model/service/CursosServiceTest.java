package model.service;

import model.domain.combinacion.curso.Curso;
import model.domain.combinacion.curso.Materia;
import model.domain.combinacion.curso.TipoCurso;
import model.domain.combinacion.curso.horario.Dia;
import model.domain.combinacion.curso.horario.Hora;
import model.domain.combinacion.curso.horario.Horario;
import model.repository.CursosRepository;
import model.repository.DefaultCursosRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class CursosServiceTest {

    private CursosRepository cursosRepository;
    private CursosService cursosService;

    @BeforeEach
    public void setup() {
        cursosRepository = new DefaultCursosRepository();
        cursosService = new CursosService(cursosRepository);
    }

    @Test
    public void obtenerCursosCursables_unCursoCursable_retornaEseCurso() {
        var horarios = List.of(new Horario(Dia.Lunes, Hora.M08a10 | Hora.M10a12));
        var cursos = List.of(new Curso(1, horarios.get(0), TipoCurso.PRESENCIAL));
        var materias = List.of(new Materia(1, "1"));
        materias.get(0).setCursos(cursos);

        var cursosCursables = cursosService.obtenerCursosCursables(materias);

        assertThat(cursosCursables).containsExactly(cursos.get(0));
    }
}

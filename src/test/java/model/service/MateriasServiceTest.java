package model.service;

import model.domain.combinacion.curso.Materia;
import model.repository.DefaultMateriasRepository;
import model.repository.MateriasRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class MateriasServiceTest {

    private MateriasRepository materiasRepository;
    private MateriasService materiasService;

    @BeforeEach
    public void setup() {
        materiasRepository = new DefaultMateriasRepository();
        materiasService = new MateriasService(materiasRepository);
    }

    @Test
    public void corregirCodigos_materiasSinCodigo_retornaMateriasConCodigo() {
        var materias = List.of(new Materia(1, "1"));
        var materiasSinCodigo = List.of(new Materia(0, "1"));

        materiasService.corregirCodigos(materiasSinCodigo, materias);

        assertThat(materiasSinCodigo.get(0).getCodigo()).isNotEqualTo(0);
    }

    @Test
    public void removerMateriasSinCodigo_materiasSinCodigo_retornaListaVacia() {
        var materiasSinCodigo = new ArrayList<>(List.of(new Materia(0, "1")));

        materiasService.removerMateriasSinCodigo(materiasSinCodigo);

        assertThat(materiasSinCodigo).isEmpty();
    }

    @Test
    public void obtenerMateriasCursables_conCorrelatividadesAprobadas_retornaMateriaOfertada() {
        var listaMaterias = List.of(
                new Materia(1, "1"),
                new Materia(2, "2", List.of(1)));
        var materiasAprobadas = List.of(new Materia(1, "1"));
        var materiasOfertadas = List.of(
                new Materia(1, "1"),
                new Materia(2, "2"));
        materiasRepository.getListaMaterias().addAll(listaMaterias);
        materiasRepository.getMateriasAprobadas().addAll(materiasAprobadas);
        materiasRepository.getMateriasOfertadas().addAll(materiasOfertadas);

        var materiasCursables = materiasService.obtenerMateriasCursables();

        assertThat(materiasCursables).containsExactly(materiasOfertadas.get(1));
    }

    @Test
    public void obtenerMateriasCursables_conMateriasSinOfertar_retornaSoloMateriaOfertada() {
        var listaMaterias = List.of(
                new Materia(1, "1"),
                new Materia(2, "2"));
        var materiasAprobadas = new ArrayList<Materia>();
        var materiasOfertadas = List.of(new Materia(1, "1"));
        materiasRepository.getListaMaterias().addAll(listaMaterias);
        materiasRepository.getMateriasAprobadas().addAll(materiasAprobadas);
        materiasRepository.getMateriasOfertadas().addAll(materiasOfertadas);

        var materiasCursables = materiasService.obtenerMateriasCursables();

        assertThat(materiasCursables).containsExactly(materiasOfertadas.get(0));
    }
}

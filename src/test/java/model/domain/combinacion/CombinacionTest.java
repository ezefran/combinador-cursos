package model.domain.combinacion;

import model.domain.combinacion.curso.Curso;
import model.domain.combinacion.curso.Materia;
import model.domain.combinacion.curso.TipoCurso;
import model.domain.combinacion.curso.horario.Dia;
import model.domain.combinacion.curso.horario.Hora;
import model.domain.combinacion.curso.horario.Horario;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class CombinacionTest {

    private boolean setUp;
    private List<Horario> horarios;
    private List<Curso> cursos;
    private List<Combinacion> combinaciones;

    @BeforeEach
    public void setUp() {
        if (setUp) return;

        horarios = new ArrayList<>();
        horarios.add(new Horario(Dia.Lunes, Hora.M08a10 | Hora.M10a12));
        horarios.add(new Horario(Dia.Martes, Hora.M08a10 | Hora.M10a12));
        horarios.add(new Horario(Dia.Miercoles, Hora.M08a10 | Hora.M10a12));
        horarios.add(new Horario(Dia.Lunes, Hora.T12a14 | Hora.T14a16));

        cursos = new ArrayList<>();
        cursos.add(new Curso(1, horarios.get(0), TipoCurso.PRESENCIAL));
        cursos.add(new Curso(2, horarios.get(0), TipoCurso.A_DISTANCIA));
        cursos.add(new Curso(3, horarios.get(1), TipoCurso.PRESENCIAL));
        cursos.add(new Curso(4, horarios.get(3), TipoCurso.PRESENCIAL));
        cursos.add(new Curso(5, horarios.get(0), TipoCurso.PRESENCIAL));

        cursos.get(0).setMateria(new Materia(0, "1"));
        cursos.get(1).setMateria(new Materia(1, "2"));
        cursos.get(2).setMateria(new Materia(2, "3"));
        cursos.get(3).setMateria(new Materia(3, "4"));
        cursos.get(4).setMateria(new Materia(3, "4"));

        combinaciones = new ArrayList<>();
        combinaciones.add(new Combinacion(cursos.get(0)));
        combinaciones.add(new Combinacion(cursos.get(1)));
        combinaciones.add(new Combinacion(cursos.get(2)));
        combinaciones.add(new Combinacion(cursos.get(0)));
        combinaciones.add(new Combinacion(cursos.get(3)));

        setUp = true;
    }

    @Test
    public void hayConflictoDeHorario_cursoMismoHorarioPeroADistancia_retornaFalse() {
        Combinacion combinacion = combinaciones.get(0);
        Curso curso = cursos.get(1);
        assertThat(combinacion.hayConflictoDeHorario(curso)).isFalse();
    }

    @Test
    public void hayConflictoDeHorario_cursoMismaHoraDistintoDia_retornaFalse() {
        Combinacion combinacion = combinaciones.get(0);
        Curso curso = cursos.get(2);
        assertThat(combinacion.hayConflictoDeHorario(curso)).isFalse();
    }

    @Test
    public void hayConflictoDeHorario_cursoMismoDiaDistintaHora_retornaFalse() {
        Combinacion combinacion = combinaciones.get(0);
        Curso curso = cursos.get(3);
        assertThat(combinacion.hayConflictoDeHorario(curso)).isFalse();
    }

    @Test
    public void hayConflictoDeHorario_cursoMismoDiaMismaHora_retornaTrue() {
        Combinacion combinacion = combinaciones.get(0);
        Curso curso = cursos.get(4);
        assertThat(combinacion.hayConflictoDeHorario(curso)).isTrue();
    }

    @Test
    public void hayConflictoDeHorario_combinacionMismoCurso_retornaTrue() {
        Combinacion combinacion = combinaciones.get(0);
        Combinacion otraCombinacion = combinaciones.get(3);
        assertThat(combinacion.hayConflictoDeHorario(otraCombinacion)).isTrue();
    }

    @Test
    public void hayConflictoDeHorario_combinacionCursoDistintoDia_retornaFalse() {
        Combinacion combinacion = combinaciones.get(0);
        Combinacion otraCombinacion = combinaciones.get(2);
        assertThat(combinacion.hayConflictoDeHorario(otraCombinacion)).isFalse();
    }

    @Test
    public void hayConflictoDeHorario_combinacionCursoDistintaHora_retornaFalse() {
        Combinacion combinacion = combinaciones.get(0);
        Combinacion otraCombinacion = combinaciones.get(4);
        assertThat(combinacion.hayConflictoDeHorario(otraCombinacion)).isFalse();
    }
}

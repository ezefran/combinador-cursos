package model.domain.combinacion.curso.horario;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class HoraTest {

    @Test
    public void contieneHora() {
        Assertions.assertTrue(Hora.contiene(Hora.T13a17, Hora.T12a14));
    }
}

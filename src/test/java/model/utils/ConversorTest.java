package model.utils;

import model.domain.combinacion.curso.TipoCurso;
import model.domain.combinacion.curso.horario.Dia;
import model.domain.combinacion.curso.horario.Hora;
import model.domain.combinacion.curso.horario.Horario;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class ConversorTest {

    @Test
    public void convertirStrAHorario_unDiaUnaHora() {
        var res = Conversor.strToHorarios("Lu08a10");
        Assertions.assertEquals(new Horario(Dia.Lunes, Hora.M08a10), res.get(0));
    }

    @Test
    public void convertirStrAHorario_dosDiasUnaHora() {
        var res = Conversor.strToHorarios("LuMa08a10");
        var exp = List.of(
                new Horario(Dia.Lunes, Hora.M08a10),
                new Horario(Dia.Martes, Hora.M08a10)
        );
        Assertions.assertEquals(exp, res);
    }

    @Test
    public void convertirStrAHorario_dosDiasDosHoras() {
        var res = Conversor.strToHorarios("Lu08a10Sa10a12");
        var exp = List.of(
                new Horario(Dia.Lunes, Hora.M08a10),
                new Horario(Dia.Sabado, Hora.M10a12)
        );
        Assertions.assertEquals(exp, res);
    }

    @Test
    public void convertirStrAHorario_cualquiera() {
        var res = Conversor.strToHorarios("Sa08a12 (Semipresencial, exclusivo para recursantes que hayan ingresado en 2014");
        var exp = List.of(
                new Horario(Dia.Sabado, Hora.M08a12)
        );
        Assertions.assertEquals(exp, res);
    }

    @Test
    public void convertirHorarioAString_diasDistintosMismoHorario() {
        var horarios = List.of(
                new Horario(Dia.Lunes, Hora.M08a12),
                new Horario(Dia.Jueves, Hora.M08a12)
        );
        var res = Conversor.horariosToString(horarios);
        Assertions.assertEquals("LuJu08a12", res);
    }

    @Test
    public void tipoCursoToString_presencial() {
        assertThat(Conversor.tipoCursoToString(TipoCurso.PRESENCIAL)).isEqualTo("Presencial");
    }

    @Test
    public void tipoCursoToString_semiPresencial() {
        assertThat(Conversor.tipoCursoToString(TipoCurso.SEMI_PRESENCIAL)).isEqualTo("Semi presencial");
    }

    @Test
    public void tipoCursoToString_aDistancia() {
        assertThat(Conversor.tipoCursoToString(TipoCurso.A_DISTANCIA)).isEqualTo("A distancia");
    }

    @Test
    public void strToTipoCurso_presencial() {
        assertThat(Conversor.strToTipoCurso("Presencial")).isEqualTo(TipoCurso.PRESENCIAL);
    }

    @Test
    public void strToTipoCurso_semiPresencial() {
        assertThat(Conversor.strToTipoCurso("Semi presencial")).isEqualTo(TipoCurso.SEMI_PRESENCIAL);
    }

    @Test
    public void strToTipoCurso_aDistancia() {
        assertThat(Conversor.strToTipoCurso("A distancia")).isEqualTo(TipoCurso.A_DISTANCIA);
    }
}

## Combinador de Cursos
Aplicación de escritorio que sirve para combinar cursos de las materias de la universidad. Tiene en cuenta las materias ya aprobadas y los horarios de cada curso, siendo que solo se forman combinaciones entre cursos de materias que se pueden cursar y que también tienen horarios que no tienen conflicto entre sí.

##### Archivos necesarios
Los archivos necesarios para cargar las materias (lista de materias y oferta de materias) se descargan de la página de la universidad en formato html. Por ahora solo están disponibles la universidad UNLaM y la carrera Ingeniería Informática.

![Demo_1](images/demo_1.png)

### Descarga y creación de ejecutable
```
git clone https://gitlab.com/ezefran/combinador-cursos.git
cd combinador-cursos
./gradlew shadowJar
```

### Ejecución
```
cd build/libs
java -jar combinador_cursos-0.0.1-all.jar
```